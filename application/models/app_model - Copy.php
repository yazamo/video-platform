<?php

/* BetterCapture Model */

class App_Model extends CI_Model{

    function insert_video(){
		
		$userID = mysql_real_escape_string($_POST['userID']);
        $video_title = mysql_real_escape_string($_POST['video_title']);
		$embed = explode("v=",$_POST['url']);
		$url = mysql_real_escape_string($embed[1]);
		
		/* Check if website new or existing then do the stuff */
		
		if($_POST['weburlnew'] != '') {
		
		$weburl = mysql_real_escape_string($_POST['weburlnew']);	
		
		$this->db->query("INSERT INTO websites VALUES('','$userID','$weburl', NOW() )");
		
		$webId = $this->db->insert_id();
		
		$this->db->query("INSERT INTO videos VALUES('','$userID','$webId','$video_title','0','$url','0','0','0',NOW(),'510','315','0', '0', 'This is CTA text', '0', '0', '0','This is an Optin Title','0')");
		
		$videoId = $this->db->insert_id();
		
		$this->db->query("INSERT INTO form_data VALUES('','$userID', '$videoId', '0','0')");
		} else {
		
		$webId = mysql_real_escape_string($_POST['weburl']);
		
		$this->db->query("INSERT INTO videos VALUES('','$userID','$webId','$video_title','0','$url','0','0','0',NOW(),'510','315','0', '0', 'This is CTA text', '0','0', '0','This is an Optin Title','0')");
		
		$videoId = $this->db->insert_id();
		
		$this->db->query("INSERT INTO form_data VALUES('','$userID', '$videoId', '0','0')");
		
		}
		
		
		 
	 }
	
	 function update_video(){
		
		$userID = $_POST['userID'];
        $videoID = $_POST['videoID'];
		//$description = $_POST['description'];
		
		$embed = explode("v=",$_POST['url']);
		$url = $embed[1];
		/* removes youtube.com etc and only use the id */
	   
	
	
		$width = mysql_real_escape_string($_POST['width']);
		$height = mysql_real_escape_string($_POST['height']);
		$autoplay = $_POST['autoplay'];
		$timeline = $_POST['timeline'];
		$hd = $_POST['hd'];
		$cta = $_POST['cta'];
		$ctatime = $_POST['ctatime'];
		$video_title = mysql_real_escape_string($_POST['video_title']);
		$ctaText = substr($_POST['ctaText'],0,46);
		
		
		
		if($_POST['ctaLink'] === '') {
			$ctaLink = '0' ;
		} else {
			$ctaLink = mysql_real_escape_string($_POST['ctaLink']);
		}
		
		/* Convert cta time to seconds*/
		 
		 if($_POST['ctatimeSwitch'] === '2') {
		
		  $ctatime = $_POST['ctatime'][0].':'.$_POST['ctatime'][1].':'.$_POST['ctatime'][2];
		  $ctatime = mysql_real_escape_string($ctatime);
		  
		 } elseif($_POST['ctatimeSwitch'] === '1' || $_POST['ctatimeSwitch'] === '0') {
			 
			$ctatime = $_POST['ctatimeSwitch'] ;
		 }
		 
		 
		 
		if(isset($_POST['opt'])) { 
		$opt = $_POST['opt'];
		$optinTitle = mysql_real_escape_string(substr($_POST['optinTitle'],0,38));
		$allowSkip = $_POST['allowSkip'];
		 
		 if($_POST['optintimeSwitch'] === '2') {
		
		  $optintime = $_POST['optintime'][0].':'.$_POST['optintime'][1].':'.$_POST['optintime'][2];
		  
		  $optintime = mysql_real_escape_string($optintime);
		
		 } elseif ($_POST['optintimeSwitch'] === '1' || $_POST['optintimeSwitch'] === '0'){
			 
			$optintime = $_POST['optintimeSwitch'] ;
		 }
		 
		
		$data = array(
			'video_title' => $video_title,
			//'description' => $description ,
			'url' => $url ,
			'width' => $width ,
			'height' => $height ,
			'autoplay' => $autoplay ,
			'timeline' => $timeline ,
			'hd' => $hd ,
			'cta' => $cta,
			'ctatime' => $ctatime,
			'opt' => $opt,
			'optintime' => $optintime,
			'ctaText' => $ctaText,
			'ctaLink' => $ctaLink,
			'optinTitle' => $optinTitle,
			'allowSkip' => $allowSkip
			 
		);
	} else {
		
		
		$data = array(
			'video_title' => $video_title,
			//'description' => $description ,
			'url' => $url ,
			'width' => $width ,
			'height' => $height ,
			'autoplay' => $autoplay ,
			'timeline' => $timeline ,
			'hd' => $hd ,
			'cta' => $cta,
			'ctatime' => $ctatime
			 
		);
		
	}
		
		
		$this->db->where('userid', $userID);
		$this->db->where('videoid', $videoID);
        $this->db->update('videos',$data);
    }
	
	 function update_form_data(){
		 
		$userID = $_POST['userID'];
        $videoID = $_POST['videoID'];
		 
		 $input_field = $_POST['input_field'][0];
		 $list_name = $_POST['input_field'][1];
		 
		 $data = array(
			 'value' => $input_field,
			 'list_name' => $list_name,
	      );
		  
		$this->db->where('userid', $userID);
		$this->db->where('videoid', $videoID);
        $this->db->update('form_data',$data);
		  
		  
		 
	 }
	
	public function get_videos()
    {
        
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('*');
		$this->db->from('videos');
        $this->db->where('userid', $userID);
        $query = $this->db->get();
        return $result = $query->result();

    }
	
	public function get_websites()
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('*');
		$this->db->from('websites');
        $this->db->where('userid', $userID);
		$this->db->order_by("websiteid", "desc");
        $query = $this->db->get();
        return $result = $query->result();
		
		
	}
	
	public function get_single_website($webID)
    {
        
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('*');
		$this->db->from('websites');
        $this->db->where('userid', $userID);
		$this->db->where('websiteid', $webID);
        $query = $this->db->get();
        return $result = $query->result();

    }
	
	public function get_projects()
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('*');
		$this->db->from('projects');
        $this->db->where('userid', $userID);
        $query = $this->db->get();
        return $result = $query->result();
		
		
	}
	
	public function get_project_videos($projectID)
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('*');
		$this->db->from('videos');
        $this->db->where('userid', $userID);
		$this->db->where('websiteid', $projectID);
        $query = $this->db->get();
        return $result = $query->result();
		
		
	}
	
	public function get_uncategorized()
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$this->db->select('*');
		$this->db->from('projects');
        $this->db->where('userid', $userID);
		$this->db->where('project_name', 'uncategorized');
        $query = $this->db->get();
        return $result = $query->result();
		
	}
	
	public function move_video($uncategorizedID, $projectID)
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$data = array(
				 'websiteid' => $uncategorizedID,
			 );
		
		
		$this->db->where('userid', $userID); 
		$this->db->where('websiteid', $projectID); 
		$this->db->update('videos', $data); 
		
	}
	
	public function delete_project($projectID)
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		 
		$this->db->where('userid', $userID);
		$this->db->where('projectid', $projectID);
		$this->db->delete('projects'); 
		
		
	}
	
	public function add_project($projectName)
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$data = array( 
			 'projectid' => '',
			 'userid' => $userID,
			 'project_name' => $projectName
	      );
		   
        $this->db->insert('projects',$data);
	}
	
	public function move_to_project($projectID, $videoID)
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$data = array( 
			 'websiteid' => $projectID, 
	      );
		   
        $this->db->where('userid', $userID); 
		$this->db->where('videoid', $videoID); 
		$this->db->update('videos', $data);
	}
	
	
	public function get_single($videoID)
    {
        
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('*');
		$this->db->from('videos');
        $this->db->where('userid', $userID);
		$this->db->where('videoid', $videoID);
        $query = $this->db->get();
        return $result = $query->result();

    }
	
	
	
	public function get_last_video()
    {
        
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('*');
		$this->db->from('videos');
        $this->db->where('userid', $userID);
		$this->db->order_by("videoid", "desc");
		$this->db->limit(1);
        $query = $this->db->get();
        return $result = $query->result();

    }
	
	
	
	public function get_embed($videoID)
    {
        
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('*');
		$this->db->from('videos');
        $this->db->where('userid', $userID);
		$this->db->where('videoid', $videoID);
        $query = $this->db->get();
        return $result = $query->result();
	}
	
	public function get_app_type()
    {
	    
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->select('api_type');
		$this->db->from('users');
        $this->db->where('id', $userID);
        $query = $this->db->get();
        return $result = $query->result();

    }
	
	public function delete_video($videoID)
    {
        
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->where('userid', $userID);
		$this->db->where('videoid', $videoID);
		$this->db->delete('videos');

    }
	
	
	public function record_count($webID) {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		 if($webID != '0') {
       $this->db->where('websiteid', $webID); 
		 }
		$this->db->where('userid', $userID);
        return $this->db->count_all_results("videos");
    }
 
    public function fetch_videos($limit, $start, $webID) {
		
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
       // $this->db->limit($limit, $start);
		
		 $this->db->where('userid', $userID);
		 if($webID != '0') {
		 $this->db->where('websiteid', $webID);
		 }
		$this->db->limit($limit, $start);
		$this->db->where('userid', $userID);
        $query = $this->db->get("videos");
		
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
   
     public function add_infusionsoft($domain, $api_key)
      {
		
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		$this->db->query("INSERT INTO infusionsoft VALUES('','$userID','$domain','$api_key')");
		
	     $data = array(
				   'api_type' => '1',
				  );
		
		
		$this->db->where('id', $userID);
		$this->db->update('users', $data); 
		
	  }
	
	  public function get_infusionsoft()
      {
	 
	    $user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$this->db->select('*');
		$this->db->from('infusionsoft');
        $this->db->where('userid', $userID);
        $query = $this->db->get();
        return $result = $query->result();
		
		
		
	}
	
	
	 public function add_cm($access_token, $refresh_token,  $expires_in, $clientID)
      {
	  
	  $user = $this->ion_auth->user()->row();
	  $userID = $user->id;
	  
	  $this->db->query("INSERT INTO campaignmonitor VALUES('','$userID','$clientID','$access_token','$refresh_token','$expires_in')");
		
	   $data = array(
				   'api_type' => '4',
				  );
		
		
		$this->db->where('id', $userID);
		$this->db->update('users', $data); 
	  
	  }
	 
	
	
	  public function add_mailchimp($token, $dc)
      {
	  
	  $this->db->query("INSERT INTO mailchimp VALUES('','','$token','$dc')");
		
	  }
	 
	 /* Function to add userId to mailchimp table after row is created */
	 
	 public function add_mailchimp_userid($userID)
     {
		
		$this->db->select_max('id');
		$this->db->from('mailchimp');
		$query = $this->db->get();
		$row = $query->row();  
	
	    $id = $row->id;
	
	    $data = array(
				   'userID' => $userID,
				  );

		$this->db->where('id', $id);
		$this->db->update('mailchimp', $data); 
		
		
		
		 $data = array(
				   'api_type' => '2',
				  );
		
		
		$this->db->where('id', $userID);
		$this->db->update('users', $data); 
     }
	
	
	
	
	 public function check_api() {
		
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		
		$this->db->select('api_type');
		$this->db->from('users');
        $this->db->where('id', $userID);
        $query = $this->db->get();
        return $check_api = $query->result();
		
		
		 
	}
	
	
	
	public function unset_api() {
		
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		
		/* Set api type to none in users table */
		
		$data = array(
               'api_type' => '0'
            );
   
		$this->db->where('id', $userID);
		$this->db->update('users', $data); 
		
		/* Delete Infusionsoft stored data */
		
		$this->db->where('userID', $userID);
		$this->db->delete('infusionsoft'); 
		
		/* Delete MailChimp stored data */
		
		$this->db->where('userID', $userID);
		$this->db->delete('mailchimp'); 
		
		/* Delete CampaignMonitor stored data */
		
		$this->db->where('userID', $userID);
		$this->db->delete('campaignmonitor'); 
		
		
		$data = array(
               'list_name' => '0',
			   'value' => '0'
            );
		
		$zero = '0';	
		 
		$this->db->where('list_name !=', $zero);
		$this->db->where('userID', $userID); 
		$this->db->update('form_data', $data); 

  
	}
	
	
	public function get_mailchimp() {
		
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$this->db->select('*');
		$this->db->from('mailchimp');
        $this->db->where('userid', $userID);
        $query = $this->db->get();
        return $result = $query->result();
		
		
	}
	
	public function get_campaignmonitor() {
		
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$this->db->select('*');
		$this->db->from('campaignmonitor');
        $this->db->where('userid', $userID);
        $query = $this->db->get();
        return $result = $query->result();
		
	}
	
	/* Change CampaignMonitor Client to pull list from */
	 public function cm_change_client($clientID) {
		 
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		 
		$data = array(
			'clientID' => $clientID
		 );
		$this->db->where('userid', $userID); 
        $this->db->update('campaignmonitor',$data);
		 
	 }
	
	
	
	 /* Adds the contact to email list based on selected API */
	
	 function add_contact($api_type, $email, $userID, $videoID) {
		 
		if($api_type === '1') {	 
		 
		 $this->db->select('*');
		 $this->db->from('form_data');
         $this->db->where('userID', $userID);
		 $this->db->where('videoID', $videoID);
         $query = $this->db->get();
         $result = $query->result();
		 
		 $listID = $result[0]->value;
		  
		 
		 $this->db->select('*');
		 $this->db->from('infusionsoft');
         $this->db->where('userID', $userID);
         $query = $this->db->get();
         $is_result = $query->result(); 
		 
		$this->load->library('infusionsoft');
		$this->infusionsoft->is_api();
		$app = new iSDK;  
		$app->cfgCon($is_result[0]->domain, $is_result[0]->api_key);   
				   
		 /* Add Contact */ 
		 
		$conDat = array('Email' => $email);
		$conID = $app->addCon($conDat);
		//$conID = $app->dsAdd("Contact", $conDat);
		
		
		/* Get most recent added Contact */
		
		$returnFields = array('Id', 'Email', 'DateCreated');
		$query = array('DateCreated' => '%');
		$tags = $app->dsQueryOrderBy("Contact", 1, 0, $query, $returnFields, 'Id', false);
				
		$contactId = $tags['0']['Id'];
		
		 
		
		/* Tag most recent added Contact */
		
		$groupId = $listID;
		$result = $app->grpAssign($contactId, $groupId);
 
		}
		
		if($api_type === '2') {	 
		
		 $this->db->select('*');
		 $this->db->from('form_data');
         $this->db->where('userID', $userID);
		 $this->db->where('videoID', $videoID);
         $query = $this->db->get();
         $result = $query->result();
		 
		 $listID = $result[0]->value;
		  
		 
		 $this->db->select('*');
		 $this->db->from('mailchimp');
         $this->db->where('userID', $userID);
         $query = $this->db->get();
         $mc_result = $query->result();
		 
		 
		 $this->load->library('mailchimp');
		 $this->mailchimp->mailchimp_api();
		 
		 $MailChimp = new \Drewm\MailChimp(''.$mc_result[0]->token.'-'.$mc_result[0]->dc.'');
		 
		 $MailChimp->call('lists/subscribe', array(
                'id'                => $listID,
                'email'             => array('email'=>$email),
                'merge_vars'        => array('FNAME'=>'', 'LNAME'=>''),
                'double_optin'      => false,
                'update_existing'   => true,
                'replace_interests' => false,
                'send_welcome'      => false,
            )); 
			 
		}
		
		
		if($api_type === '4') {	
		    
		 $this->db->select('*');
		 $this->db->from('form_data');
         $this->db->where('userID', $userID);
		 $this->db->where('videoID', $videoID);
         $query = $this->db->get();
         $result = $query->result();
		 
		 $listID = $result[0]->value;
		  
		 
		 $this->db->select('*');
		 $this->db->from('campaignmonitor');
         $this->db->where('userID', $userID);
         $query = $this->db->get();
         $cm_result = $query->result();
		 
		 $this->load->library('campaignmonitor');
		 $this->campaignmonitor->cm_api();
		 
		 
		 $auth = array(
			'access_token' => ''.$cm_result[0]->access_token.'',
			'refresh_token' => ''.$cm_result[0]->refresh_token.'');
		$wrap = new CS_REST_Subscribers(''.$listID.'', $auth);
		$result = $wrap->add(array(
			'EmailAddress' => ''.$email.'',
			 //'Name' => 'derps derp',
			'Resubscribe' => true
		));
			
		
		}
		 
		 
	 }
	 
	 
	 public function get_form_list($videoID) {
		 
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$this->db->select('*');
		$this->db->from('form_data');
        $this->db->where('userid', $userID);
		$this->db->where('videoid', $videoID);
        $query = $this->db->get();
        return $result = $query->result();
		 
		 
	 }
	 
	 
	 public function add_category($catName)
    {
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
		
		$data = array( 
			 'websiteid' => '',
			 'userid' => $userID,
			 'weburl' => $catName,
			 'date' => '0'
	      );
		   
        $this->db->insert('websites',$data);
	}
	 
	  
	
 }
 
 ?>