<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* BetterCapture Controller */

class App extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
        $this->load->library("pagination");
		$this->load->database();
		$this->load->model("app_model");
 		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('auth');
		$this->load->helper('language');
		$this->load->library('session');
		ini_set('display_errors', 1);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0"); 
		$this->output->set_header("Pragma: no-cache");
		
		
		
		
		
	}

	 
	public function index()
	{
	  
	  if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page if not logged in
			redirect('/login', 'refresh');
		}
	     
		   //redirect them to user dashboard if they are logged in
		 elseif ($this->ion_auth->logged_in()) {
			 redirect('/videos', 'refresh');
		 }
		 
		 
		  
		 
	}
	
	public function logout()
	{ 
	    //logout user and redirect them to login page
	  $this->ion_auth->logout();
	  
	 
	   
	  redirect('app', 'refresh');
	
	}
	
	public function register()
	{
		//registration form process
		$this->data['title'] = "Create User";

		if ($this->ion_auth->logged_in() || $this->ion_auth->is_admin())
		{
			redirect('/app', 'refresh');
		}
		
		 
		
		$tables = $this->config->item('tables','ion_auth');
		
		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique['.$tables['users'].'.email]');
		$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'required|xss_clean');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email    = strtolower($this->input->post('email'));
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
				'company'    => $this->input->post('company'),
				 
			);
		}
		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data))
		{
			//check to see if we are creating the user
			//redirect them back to the admin page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("/login", 'refresh');
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['company'] = array(
				'name'  => 'company',
				'id'    => 'company',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('company'),
			);
			
			$this->data['password'] = array(
				'name'  => 'password',
				'id'    => 'password',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name'  => 'password_confirm',
				'id'    => 'password_confirm',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

			
			
			
		//$this->load->view('template/standard');
        $this->load->view('register', $this->data);
        //$this->load->view('template/footer'); 
		}
	}
	
	
	
	
	
	public function login()
	{
	  
	  if ($this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('/', 'refresh');
		} else {
			
			//redirect('/login', 'refresh');
			
		}
		
			
	
	//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('/', 'refresh');
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);

		 
	    //$this->load->view('template/standard');
        $this->load->view('login', $this->data);
        //$this->load->view('template/footer'); 
	
	}
}
	
	


  public function create_video() { 
  
    if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page if not logged in
			redirect('/login', 'refresh');
		}
  
     $this->load->model('app_model');
     $data['websites'] = $this->app_model->get_websites();
      
	 $data['title'] = 'Create a new video';
     //$this->load->view('template/header'); 
	 $this->load->view('create_video', $data);
	 //$this->load->view('template/footer');    
  }


    function add_video(){
	
	  if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page if not logged in
			redirect('/login', 'refresh');
		}
    
	$this->load->model('app_model');
    $this->app_model->insert_video(); 
	
	
	$data['result'] = $this->app_model->get_last_video();
    redirect('/video/id/'.$data['result'][0]->videoid.'', 'refresh');
	
	
	
}
   
	

   public function video() { 
      
	    if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page if not logged in
			redirect('/login', 'refresh');
		}
      
      
	  $this->load->model('app_model');
	  $data['alert'] = '';
      
	  if(isset($_POST['submit'])) {
		 
	 
      $this->app_model->update_video();  
	  $data['alert'] = '<div class="alert alert-success">
  				Changes Saved
 				  
		</div>';
	  }
	  
	  
	  if(isset($_POST['input_field'])) {
		 
		 $this->app_model->update_form_data(); 
	  }
   
     $get = $this->uri->ruri_to_assoc();
	 $videoID = $get['id']; 
	 
	 
     $data['result'] = $this->app_model->get_single($videoID); 
	 $data['api_type'] = $this->app_model->get_app_type(); 
	 
	  
	 
	 $get_api = $this->app_model->check_api(); 
	 
	   if($get_api[0]->api_type != '0') {
		   
		   $data['current_list'] = $this->app_model->get_form_list($videoID);
	  }
		
	 if($get_api[0]->api_type === '1') {
		 
		 /* hit infusionsoft api */
	 
	    $appdata = $this->app_model->get_infusionsoft(); 
	 
	    $this->load->library('infusionsoft');
		$this->infusionsoft->infusion_api();
		 
        
		$app = new iSDK;
		
		$app->setToken($appdata[0]->access_token);
		 
	 
 		$returnFields = array('Id','GroupName');
		$query = array('Id' => '%');
		$data['tags'] = $app->dsQuery("ContactGroup",10,0,$query,$returnFields);
		 
		
		
		
		
		
		 
		/* End infusionsoft api */
		 
	 } elseif($get_api[0]->api_type === '2') {
		 
		 /* hit mailchimp api */
	 
	     $appdata = $this->app_model->get_mailchimp(); 
		
		
		 $this->load->library('mailchimp');
		 $this->mailchimp->mailchimp_api();
		 
		 $MailChimp = new \Drewm\MailChimp(''.$appdata[0]->token.'-'.$appdata[0]->dc.'');
		 $data['getUserList'] = $MailChimp->call('lists/list');
		 
		 
		 /* End mailchimp api */
		 
		 
		 
	 } elseif($get_api[0]->api_type === '3') {
		
		 /*hit cc api */
		 
		$this->load->library('constantcontactapi');
		$this->constantcontactapi->cc_api();
		
		$apikey = "6w9f6ezm2a7waex63nefwvbn";
		 
		$appdata = $this->app_model->get_constantcontact(); 
		 
		$ConstantContact = new ConstantContact ("oauth2", $apikey, $appdata[0]->username, $appdata[0]->token);
		 
		$ContactLists = $ConstantContact->getLists();
		
		$data['ccLists'] = $ContactLists;
						 
	 
	  /* hit campaignmonitor api */
		 
     } elseif($get_api[0]->api_type === '4') {
		 
		 
		
		$appdata = $this->app_model->get_campaignmonitor(); 
		
		$this->load->library('campaignmonitor');
		$this->campaignmonitor->cm_api();
		
		
		$auth = array(
          'access_token' => $appdata[0]->access_token,
          'refresh_token' => $appdata[0]->refresh_token
        );
        $cs = new CS_REST_General($auth);
        $clients = $cs->get_clients()->response;
 
	 
		$data['cmClients'] = $clients;
		
		
		 
		$wrap = new CS_REST_Clients(''.$appdata[0]->clientID.'',$auth);
		
		 $results = $wrap->get_lists();
		
		 $data['getCMLists'] = $results->response;
		 
		 
		
		
		/* End ampaignmonitor api */
		 
	 } else {
		 
		/* No API */ 
	 }
	 
	 
	 
	 
	 
	 
	 
     $data['title'] = 'Edit Video';
    // $this->load->view('template/header'); 
	 $this->load->view('single', $data);
	 //$this->load->view('template/footer');  
	 
	 
	 
   
   }
   
   /* Change CampaignMonitor Client to pull list from */
   
   public function cm_change_client() {
	   
	    $clientID = $_POST['clientID']; 
	    $appdata = $this->app_model->cm_change_client($clientID); 
	   
   }
   
   public function videos()
	{
	  
	  if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page if not logged in
			redirect('/login', 'refresh');
	  } 
	
	
	
	
	/* Logic for filter + pagination to work together. */
	
	
	 
	  $webID = ''; 
	   	
		
	  if(!isset($webID) & !$webID < '0') {
       $this->session->set_userdata('webID', '0');		
	 } elseif(isset($_POST['checkwebID'])) {
		$checkwebID = $_POST['checkwebID']; 
		$this->session->set_userdata('webID', $checkwebID);	
	 }  
	 
	   $webID = $this->session->userdata('webID');
		 
	 
	 
			
		
 		   
		
		 //pagination config
		
		
		
		$config = array();
        $config["base_url"] = base_url() . "videos";
		
		 
		    
		$config["total_rows"] = $this->app_model->record_count($webID);  
		$config["per_page"] = 6;
        $config["uri_segment"] = 2; 
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="current"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		//$config["total_rows"] = $this->app_model->record_count();
		 
		 
		$this->pagination->initialize($config);
		 
		$page = ($this->uri->rsegment(3)) ? $this->uri->rsegment(3) : 0;
		
		 
        $data["results"] = $this->app_model->fetch_videos($config["per_page"], $page, $webID);
		 
        $data["links"] = $this->pagination->create_links();
    
		 $this->load->model('app_model');
         $data['websites'] = $this->app_model->get_websites(); 
		 $data['websingle'] = $this->app_model->get_single_website($webID);
		 
		$data['title'] = "Your Videos";
		 //$this->load->view('template/header');
		$this->load->view('videos', $data);
		 //$this->load->view('template/footer');
	}
	
	public function api()
	{
		
		$this->load->view('api');
	}
	
	  public function embed()
	  {
		 $this->load->view('embed'); 
		  
	  }
	  
	  
	  public function preview()
	  {
		 $this->load->view('preview'); 
		  
	  }
	  
	  public function test_youtube_video()
	  {
		 $this->load->view('test_youtube_video'); 
		  
	  }
	  
	  public function mini_preview()
	  {
		 $this->load->view('mini_preview'); 
		  
	  }
	
	public function generate_embed_code()
	  {
		 $videoID = $_POST['videoid'];
		 $this->load->model('app_model');
         $data['result'] = $this->app_model->get_embed($videoID); 
		 $data['api_type'] = $this->app_model->get_app_type(); 
		 $this->load->view('embed_code', $data); 
	  }
	  
	  public function preview_video()
	  {
		 $videoID = $_POST['videoid'];
		 $this->load->model('app_model');
         $data['result'] = $this->app_model->get_embed($videoID); 
		 $data['api_type'] = $this->app_model->get_app_type(); 
		 $this->load->view('preview_video', $data); 
	  }
	  
	  
	 public function delete_video()
	  {
		 
		 
		$videoID = $_POST['videoid'];
		$this->load->model('app_model');
        $data['result'] = $this->app_model->delete_video($videoID); 
		 
		 
	    $data['message'] = 'Video Deleted'; 
		 
		  
	  }
	  
	 
	   public function settings()
	   {
		   
		 
		   if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page if not logged in
			redirect('/login', 'refresh');
		}
		 
		 
		 /* Load mailchimp OAuth2 Client */
		   
		 $this->load->library('mailchimp');
		 $this->mailchimp->mc_auth();
		 $data['client'] = new MC_OAuth2Client();
		 
		 
		 $this->load->model('app_model');
         $data['api'] = $this->app_model->check_api(); 
		
		
		/* Check user api */
		 
		 if($data['api']['0']->api_type === '0') {
			 
			$data['api_type'] = $this->load->file('application/views/providers/none.php', true);
			
	     /* IS api */		
			 
		 } elseif($data['api']['0']->api_type === '1') {
			 
			 
			$data['api_type'] = $this->load->file('application/views/providers/infusionsoft.php', true);
			 
		 /* MC api */		 
		 
		 } elseif($data['api']['0']->api_type === '2') {
			 
			 $data['api_type'] = $this->load->file('application/views/providers/mailchimp.php', true);
			 
		 /* CC api */	
		  	 
		 } elseif($data['api']['0']->api_type === '3') {
			 
			 $data['api_type'] = $this->load->file('application/views/providers/constantcontact.php', true);
			 
			 
		} elseif($data['api']['0']->api_type === '4') {
			 $data['api_type'] = $this->load->file('application/views/providers/campaignmonitor.php', true);
		}
		 
		 $data['title'] = "Account Settings";
		 //$this->load->view('template/header');
		 $this->load->view('settings', $data);
		 //$this->load->view('template/footer');
		   
		   
		   
		   
	   }
	   
	   
	 
	  /* Oauth2 for mailchimp */
	  public function mailchimp() {
		  
		 $this->load->library('mailchimp');
		 $this->mailchimp->mailchimp_api();
		 $data['client'] = new MC_OAuth2Client();
		 
		  
	  }
	  
	  
	  /* Oauth2 for campaign monitor */
	  
	  public function campaignmonitor() {
		  
		 $this->load->library('campaignmonitor');
		 $this->campaignmonitor->cm_api();
		 
	  $authorize_url = CS_REST_General::authorize_url(
      '100058',
      'https://dev.bettercapture.com/add_cm/',
      'ImportSubscribers,ManageLists');
	  header('Location: '.$authorize_url);
  
	  }
	  
	  public function add_cm() {
		  
		 $this->load->library('campaignmonitor');
		 $this->campaignmonitor->cm_api();
		 
		$client_code = $_GET['code'];
 
		$client_id = 100058;
		$client_secret = '1Hs51R1RMcSo7n1PEc1m11a19e115o18M12OuY4voafihqp11i17gOdznwr71ebPfX61b1VCZ11MAei1';
		$redirect_uri = 'https://dev.bettercapture.com/add_cm/';
		$code = $client_code;
		
		$result = CS_REST_General::exchange_token($client_id, $client_secret, $redirect_uri, $code);
		
		if ($result->was_successful()) {
        $access_token = $result->response->access_token;
        $expires_in = $result->response->expires_in;
        $refresh_token = $result->response->refresh_token;
 
        $auth = array(
          'access_token' => $access_token,
          'refresh_token' => $refresh_token
        );
         
		 $cs = new CS_REST_General($auth);
         $clients = $cs->get_clients()->response;

         $clientID = $clients[0]->ClientID;
		
		  
	    $this->load->model('app_model');
        $this->app_model->add_cm($access_token, $refresh_token,  $expires_in, $clientID); 
		
		
		

		} else {
			$response = 'An error occurred:<br/>';
			$response .= $result->response->error.': '.$result->response->error_description."<br/>";
		}
		//echo $response;
	
		// echo $clientID;
		redirect('/', 'refresh');
	  }
	  
	  
	  
	  public function add_mailchimp() {
		  
		
	     $this->load->library('mailchimp');
		 $this->mailchimp->mc_auth();
		 $client = new MC_OAuth2Client();
		
		$session = $client->getSession();
		$rest = new MC_RestClient($session);
        $mcdata = $rest->getMetadata(); 
		  
		  
		 $token = $session['access_token'];
		 $dc = $mcdata['dc'];
		 
	 
		 $this->load->model('app_model');
         $this->app_model->add_mailchimp($token, $dc); 
		 
	 
		 
		 redirect('/add_mailchimp_user'); 
	  }
	  
	  
	  public function add_mailchimp_user() {
		  
		$user = $this->ion_auth->user()->row();
		$userID = $user->id;
	 
		$this->load->model('app_model');  
		$this->app_model->add_mailchimp_userid($userID);   
		
		
		 redirect('/settings'); 
	  }
	  
	  
	    public function add_infusionsoft() {
		   
		 $this->load->library('infusionsoft');
		 $this->infusionsoft->infusion_api();
		 
		  $app = new iSDK();
		  
		  $code = $this->input->get('code', TRUE);

	 
			$app->setClientId('mmqtwx75tm7n4cce2wzqpaaa');
			$app->setSecret('K4gk7NWsMu');
			
			$access_token = $app->authorize($code)->access_token;
			
			$this->app_model->add_infusionsoft($access_token);  
		   redirect('/settings'); 
	   }
	 
	  
	  
	   public function unset_api() {
		   
		   $this->app_model->unset_api(); 
		   redirect('/settings', 'refresh');
		   
	   }
	   
	   
	   public function constantcontact() {
		   
		    //$this->load->library('constantcontact');
		    //$this->constantcontact->cc_api();
			
			//$username = "jack@yazamo.com";
			//$accessToken = "6503106c-69bc-4b18-8829-7e39ee306af7";
			$apikey = "6w9f6ezm2a7waex63nefwvbn";
			
			
		 $consumersecret = "rkTBdruAECdzadPXStXtEtaZ";
			
			
		 $verificationURL = 'http://dev.bettercapture.com/';
		 $returnURL = 'http://dev.bettercapture.com/add_cc/';
		 $theRequest = 'http://dev.bettercapture.com/add_cc/';
			
			redirect("https://oauth2.constantcontact.com/oauth2/oauth/siteowner/authorize?response_type=code&client_id=".$apikey."&redirect_uri=".$theRequest."");
			
		  
		    //$this->load->view('test');
	   }
	   
	    public function add_cc() {
			
			
			
			$apikey = "6w9f6ezm2a7waex63nefwvbn";
			$consumersecret = "rkTBdruAECdzadPXStXtEtaZ";
			$verificationURL = 'http://dev.bettercapture.com/';
			$returnURL = 'http://dev.bettercapture.com/add_cc/';
			//$theRequest = 'http://dev.bettercapture.com/add_cc/';
			
			$code = $_REQUEST['code'];
			$username = $_REQUEST['username'];
			
			//We will use PHP cURL to make a request to Constant Contact to get the Access Token.
			
			$rqurl = "https://oauth2.constantcontact.com/oauth2/oauth/token?grant_type=authorization_code&client_id=$apikey&client_secret=$consumersecret&code=$code&redirect_uri=$returnURL";
			$rq = curl_init();
			
			curl_setopt($rq, CURLOPT_URL, $rqurl);
			curl_setopt($rq, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($rq, CURLOPT_BINARYTRANSFER, 1);
			curl_setopt($rq, CURLOPT_HEADER, 0);
			curl_setopt($rq, CURLOPT_SSL_VERIFYPEER, 0);
			
			if (!$result = curl_exec($rq)) {
				//If there is an error, dump it so we can see it.
				echo curl_error($rq);
			} else {
				//echo "<pre>Access Token Information:\n";
				//print_r($result);
				$obj = json_decode($result); // The Result of this request is returned as JSON, so we need to parse it.
				//Create the User Ojbect.
				$user['username'] = $username;
				$access_token = $obj->access_token;
				$expires = $obj->expires_in;
				
				
				//echo $username;
				//var_dump($obj->access_token);
				
			    $this->app_model->add_cc($username, $access_token, $expires);
				 
			}
			
			curl_close($rq);
			
			redirect("http://dev.bettercapture.com/videos", "refresh");
			
 	
		}
	  
	   
	   /* Video Form hits this function and process data accordingly */
	   
	    public function post_form_data() {
			
			$email = $_POST['email'];
			$api_type = $_POST['api_type'];
			$userID = $_POST['userID'];
			$videoID = $_POST['videoID'];
			$datestamp = $_POST['datestamp'];
			 
			
			$result = $this->app_model->add_contact($api_type, $email, $userID, $videoID, $datestamp); 
			
			 
			
		}
		
		
		public function projects() {
			
			$this->load->model('app_model');
     		$data['results'] = $this->app_model->get_projects();
			$this->load->view('projects', $data);
		}
		
		public function get_project_videos() {
			
			$projectID = $_POST['projectid'];
			
			$this->load->model('app_model');
     		$data['results'] = $this->app_model->get_project_videos($projectID);
			$data['projects'] = $this->app_model->get_projects();
			$this->load->view('project_videos', $data);
		}
		
		public function delete_project() {
			
		  $projectID = $_POST['projectid']; 
		  $this->load->model('app_model');
		  $data['result'] = $this->app_model->get_uncategorized(); 
		  $uncategorizedID = $data['result'][0]->projectid;
		  $this->app_model->move_video($uncategorizedID, $projectID);
          $this->app_model->delete_project($projectID); 
		}
		
		public function add_project() {
			
			$projectName = $_POST['project_name'];
			$this->load->model('app_model');
			$this->app_model->add_project($projectName);
		}
		
		public function move_to_project() {
			
			$projectID = $_POST['projectid'];
			$videoID = $_POST['videoid'];
			$this->load->model('app_model');
			$this->app_model->move_to_project($projectID, $videoID);
		}
		
		
		
		public function add_category() {
			
			$catName = $_POST['category_name'];
			$this->load->model('app_model');
			$this->app_model->add_category($catName);
		}
		
		
		public function text()
		{
			
			$this->load->view('project_text'); 
		}
		
		
		public function full_preview()
		{
			
			
			$get = $this->uri->ruri_to_assoc();
	 		$videoID = $get['id']; 
	 
	 		//print_r($get = $this->uri->ruri_to_assoc());
            $data['result'] = $this->app_model->get_single($videoID); 
			$data['api_type'] = $this->app_model->get_app_type();
			$this->load->view('full_preview', $data);
			 
		}
		
	 function _render_page($view, $data=null, $render=false)
    {

        $this->viewdata = (empty($data)) ? $this->data: $data;

        $view_html = $this->load->view($view, $this->viewdata, $render);

        if (!$render) return $view_html;
    }
	
	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
		
		function forgot_password()
	{
		$this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required');
		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('/forgot_password', $this->data);
		}
		else
		{
			// get identity for that email
            $identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
            if(empty($identity)) {
                $this->ion_auth->set_message('forgot_password_email_not_found');
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("/forgot_password", 'refresh');
            }
            
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("/forgot_password", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				//render
				$this->_render_page('/reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						$this->logout();
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("/forgot_password", 'refresh');
		}
	}
	  
       public function analytic_dashboard() {
		    
		   $data['videoID'] = $this->app_model->get_last_video();
		   redirect('/analytics/id/'.$data['videoID'][0]->videoid.'/chart/unique_views');
		 }
	 
	 
	  public function analytics() {
		
		 $get = $this->uri->ruri_to_assoc();
		$videoID = $get['id'];
		
		 
		
		
		$data['videos'] = $this->app_model->get_videos();
		$data['video'] = $this->app_model->get_single($videoID);   
		$data['unique'] = $this->app_model->count_unique($videoID);
		
		/*if(isset($_POST['chartData'])) {
		 $data['chartData'] = $_POST['chartData'];
	 
		 
		} else {
		  $data['chartData'] = 'unique_views';
		 
		 
		}*/
		  
		if(isset($_POST['startDate'])) {
		
		$startDate = $_POST['startDate'];
		$endDate = $_POST['endDate'];
		
		$data['startDate'] = $_POST['startDate'];
		$data['endDate'] = $_POST['endDate'];
			 
		$data['analytics'] = $this->app_model->get_analytics_date($videoID, $startDate, $endDate);
		 	  
			 
		 } else {
		
		$data['analytics'] = $this->app_model->get_analytics($videoID);
		 
		
		}
		
		
		
		 $this->load->view('analytics', $data);  
	  }
	  
	  
	  public function chart_data() {
		/*$get = $this->uri->ruri_to_assoc();
	    $videoID = $get['id']; 
		
		 
		$data['result'] = $this->app_model->chart_data($videoID);
		 */
		$this->load->view('chart_data');  
	  
	  }
	  
	  
	function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->_render_page('/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('/change_password', 'refresh');
			}
		}
	}
	
	
	public function add_analytics_row() {
		
		
		$this->load->model('app_model');
	    
		$this->app_model->add_analytics_row();  
		
		
		
	}
	  
	  
	  
	   public function tester() {
		
		 /* tester function */
		
		/**$email = 'test@yazamo.com';
		$videoID = '68';
		$userID = '2';
		  
		 $this->db->select('*');
		 $this->db->from('form_data');
         $this->db->where('userID', $userID);
		 $this->db->where('videoID', $videoID);
         $query = $this->db->get();
         $result = $query->result();
		 
		 $listID = $result[0]->value;
		
		 $this->db->select('*');
		 $this->db->from('constantcontact');
         $this->db->where('userID', $userID);
         $query = $this->db->get();
         $cc_result = $query->result();
		 
		 $this->load->library('constantcontactapi');
		 $this->constantcontactapi->cc_api();
		 
		 $apikey = "6w9f6ezm2a7waex63nefwvbn";
		 
		 $ConstantContact = new ConstantContact ("oauth2", $apikey, $cc_result[0]->username, $cc_result[0]->token);
		 
		$Contact = new Contact();
        $Contact->emailAddress = $email;
        //$Contact->firstName = $_POST['first_name'];
		
		$Contact->lists = array("$listID");
		$ConstantContact->addContact($Contact);
		$ContactLists = $ConstantContact->getLists();
		
		
		echo "<pre>";
		print_r($listID);
		echo "</pre>";**/
		
		$this->load->view('test');
		}
	  
	  
	  
}
 