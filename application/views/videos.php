<?php $user = $this->ion_auth->user()->row(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Video Library</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
   
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
     <div class="collapse navbar-collapse">
      <div class="pull-right">
      <ul class="nav navbar-nav navbar-right">
       <li class="mainLink"><a href="/create_video">Add Video</a></li>
       <li class="mainLink"><a href="/" class="current">Video Library</a></li>
      <!-- <li class="mainLink"><a href="/projects">Projects</a></li>-->
       <li class="mainLink"><a href="/analytic_dashboard">Analytics</a></li>
       <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
        <li class="dropdown register-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <!--<img class="userIcon" src="<?php //echo $base;?>images/userIcon.png"/>&nbsp;&nbsp;--> <?php echo $user->first_name;?> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="/settings"><img src="<?php echo $base;?>images/settingsIcon.jpg"/>&nbsp;&nbsp;Settings</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/helpIcon.jpg"/>&nbsp;&nbsp;Help</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="/logout"><img src="<?php echo $base;?>images/logoutIcon.jpg"/>&nbsp;&nbsp;Logout</a></li>
          </ul>
        </li>
       
      </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
 
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="filterBar">
<div class="container-fluid">
<div class="col-md-8 col-xs-8">
<div class="text-left">
<h1 class="pageTitle">Your Videos : <small><?php if(empty($websingle[0]->weburl)) { ?> All <?php } else { echo $websingle[0]->weburl; }?></small></h1>
</div>
</div>
<!--<pre>
<?php  //$webID = $this->session->userdata('webID');?>
<?php //var_dump($links);?>
</pre>-->
<div class="col-md-4 col-xs-4">
<form class="form-horizontal filterForm" role="form" id="filterVideos" action="/videos" method="post">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 col-md-4 control-label text-right">Filter Videos</label>
    <div class="col-sm-8 col-md-8 col-xs-8 text-right">
     <select name="checkwebID" id="checkwebID" onchange='this.form.submit()'>
          <option value="">Filter by Category</option> 
             <option value="0">ALL</option>
             <?php foreach($websites as $data) : ?>
               <option value="<?php echo $data->websiteid; ?>"><?php echo $data->weburl; ?></option>
             <?php endforeach;?>
            </select>
    </div>
  </div>
 </form>
</div>
</div>
</section>
<section class="mainSection">
<div class="container-fluid">
<?php if($results === false)  { ?>
<div class="text-center">
       <h3 class="addvid">You have no videos. Please <a href="/create_video">add a video</a> to get started.</h3>
</div>
<?php } else { ?>

<?php foreach(array_chunk($results, 2, true) as $array) { ?>
<div class="row mainVidRow">
<?php foreach($array as $data) { ?>
<div class="col-sm-6 col-md-6 col-xs-6 videoCol" id="videoBox-<?php echo $data->videoid; ?>">
   <div class="col-sm-5 col-md-5 col-xs-5" style="padding:0;"><div class="vidThumb text-center"><a id="<?php echo $data->videoid; ?>" href="/video/id/<?php echo $data->videoid; ?>"><img src="http://img.youtube.com/vi/<?php echo $data->url; ?>/0.jpg"/></a></div></div>
   <div class="col-sm-7 col-md-7 col-xs-7 videoDataWrap">
   <div class="videoData">
   <h3 class="videoTitle"><a href="/video/id/<?php echo $data->videoid; ?>"><?php echo stripslashes($data->video_title); ?></a></h3>
   <p class="timeStamp"><span><?php echo date("M Y", strtotime($data->datetime));?></span></p>
   <p class="videoDesc"><?php if($data->description !='0' && $data->description != '') { ?> <?php echo stripslashes($data->description);?><?php }  else { ?> <?php echo 'This video has no description'; ?><?php }?></p>
     <div class="btn-group videoControls">
       		  <a href="/video/id/<?php echo $data->videoid; ?>" class="btn btn-default rLightBorder">Edit</a>
              <a href="/analytics/id/<?php echo $data->videoid; ?>/chart/unique_views" class="btn btn-default lLightBorder rLightBorder">Analytics</a></dd>
              <a id="<?php echo $data->videoid; ?>" class="getEmbed btn btn-default rLightBorder lLightBorder"  data-toggle="modal" data-target="#embedModal">Embed</a>
              <a id="<?php echo $data->videoid; ?>" class="deleteEmbed btn btn-default  lLightBorder">Delete</a>
      </div><!--/btn-group-->
      </div>
   </div><!--/col-sm-8 col-md-8-->
 </div><!--/col-sm-4 col-md-4-->
 <?php } ?>
 </div><!--/mainVidRow-->
 <?php } ?>	
<?php } ?>
<div class="row">
<div class="text-center">
<?php echo $links; ?>
</div>
</div>
<div class="push"></div>
</div>
</div>
</section>
<footer style="background:#f9fcfd;border-top:1px solid #dddfdf;">
<div class="container-fluid">  
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div>
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<a href="http://www.yazamo.com/" target="_blank"><img src="<?php echo $base;?>images/copyright.jpg"/></a>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
 </footer> 
<!-- Embed Modal -->
<div class="modal fade" id="embedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Embed Code</h4>
      </div>
      <div id="embedCode" class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary">Copy</button>-->
      </div>
    </div>
  </div>
</div>


<!-- Preview Modal -->
<div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closeVideo" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Video Preview</h4>
      </div>
      <div id="previewVid" class="modal-body">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default closeVideo" data-dismiss="modal">Close</button>
     
      </div>
    </div>
  </div>
</div>


 
 
</div>
<div class="helpBtn">
<a href="#"></a>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
 <script>
/* Embed code AJAX Call */
$('.getEmbed').click(function(e){
   var id = $(this).attr('id');
   e.preventDefault();
   $.ajax({
	    type: "POST",
        url: "/generate_embed_code",
		data: { 'videoid': id },
        success: function(result) {
        var html = jQuery('#embedCode').html(result);
      },
   });
});


/* Delete Video AJAX Call */
$('.deleteEmbed').click(function(e){
	var answer = confirm ("Are you sure you want to delete this video?");
	var id = $(this).attr('id');
	if (answer)
	  {
	   e.preventDefault();
	   $.ajax({
			type: "POST",
			url: "/delete_video",
			data: { 'videoid': id  },
			success: function() {
			$( "#videoBox-"+id ).fadeOut( "slow", function() {
			 });
			$('.wrapper').load('/videos .wrapper', function() {
			 });
			},
		});
      }
});


/* Embed code AJAX Call */
/*
$('.previewVideo').click(function(e){
   var id = $(this).attr('id');
   e.preventDefault();
   $.ajax({
	    type: "POST",
        url: "/preview_video",
		data: { 'videoid': id },
        success: function(result) {
        var html = jQuery('#previewVid').html(result);
      },
   });
});
*/

$( ".closeVideo" ).click(function() {
  $( "#wd_id" ).remove();
});
</script>
<script type="text/javascript">
 (function() {
   var s = document.createElement("script");
   s.type = "text/javascript";
   s.async = true;
   s.src = '//api.usersnap.com/load/b757bce7-66b1-48d4-afe7-f6d9a6663289.js';
   var x = document.getElementsByTagName('head')[0];
   x.appendChild(s);
 })();
</script>
</body>
</html>



