<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="">
    <title>yazamo vp</title>
    <!-- Bootstrap core CSS -->
    <link href="/vp/assets/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
	 body {
		 padding-top:0;
	 }
	 .wrap {
		 width:950px;margin:20px auto;
	 }
	 .pageTitle {
		 margin-top:0; 
	 }
	 
	  span.date {
			font-size:14px;  
	  }
	  
	  .videoButtons {
			margin-top:.5em;  
	  }
	  
	  .videoRow p {
		  margin-top:1em; 
	  }
	  
	  .container {
		 
		}
		
	.tab-pane {
		min-height:250px;	
	}
		
		div#cta {
	position: absolute;
	z-index:50000;
	background:#FFF;	
	bottom:.1em;
	text-align: center;
	padding-top:1em; 
	width: 99%;
}

	
	div#opt {
		position:absolute;
		 
		width:100%;	
		height:100%;	
		top:0;
		padding:0;
		left:0%;
		background:#CCC;
	}
	
	div#opt form {
		background: #FFF;
		padding: 1em;
		margin: 1em;
	}

	 
	 
	button, .button {
cursor: pointer;
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-weight: normal;
line-height: normal;
margin: 0 0 1.25rem;
position: relative;
text-decoration: none;
text-align: center;
display: inline-block;
padding-top: 1rem;
padding-right: 2rem;
padding-bottom: 1.0625rem;
padding-left: 2rem;
font-size: 1rem;
background-color: #008cba;
border-color: #0079a1;
color: #fff;
-webkit-transition: background-color 300ms ease-out;
-moz-transition: background-color 300ms ease-out;
transition: background-color 300ms ease-out;
padding-top: 1.0625rem;
padding-bottom: 1rem;
-webkit-appearance: none;
border: none;
font-weight: normal !important;
} 
button.alert, .button.alert {
background-color: #f04124;
border-color: #ea2f10;
color: #fff;
}

.ilightbox-holder .ilightbox-wrapper {

overflow: hidden !important;
}	
.ge{font-style:italic}.gr{color:red}.gh{color:#030}.gi{background-color:#CFC;border:1px solid #0C0}.go{color:#AAA}.gp{color:#009}.gu{color:#030}.gt{color:#9C6}.kc{color:#069}.kd{color:#069}.kn{color:#069}.kp{color:#069}.kr{color:#069}.kt{color:#078}.m{color:#F60}.s{color:#d44950}.na{color:#4f9fcf}.nb{color:#366}.nc{color:#0A8}.no{color:#360}.nd{color:#99F}.ni{color:#999}.ne{color:#C00}.nf{color:#C0F}.nl{color:#99F}.nn{color:#0CF}.nt{color:#2f6f9f}.nv{color:#033}.ow{color:#000}.w{color:#bbb}.mf{color:#F60}.mh{color:#F60}.mi{color:#F60}.mo{color:#F60}.sb{color:#C30}.sc{color:#C30}.sd{color:#C30;font-style:italic}.s2{color:#C30}.se{color:#C30}.sh{color:#C30}.si{color:#A00}.sx{color:#C30}.sr{color:#3AA}.s1{color:#C30}.ss{color:#FC3}.bp{color:#366}.vc{color:#033}.vg{color:#033}.vi{color:#033}.il{color:#F60}.css .o,.css .o+.nt,.css .nt+.nt{color:#999} 
	</style>
    
 <script type="text/javascript" src="/vp/assets/js/jquery.js"></script>

  <!-- lightbox -->
<!--<script type="text/javascript" src="/vp/assets/src/js/jquery.requestAnimationFrame.js"></script>
<script type="text/javascript" src="/vp/assets/src/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/vp/assets/src/js/ilightbox.packed.js"></script>
<link rel="stylesheet" type="text/css" href="/vp/assets/src/css/ilightbox.css"/>
<!-- /lightbox-->
  </head>
 <body>
 <div class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">BetterCapture</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
           <li><a href="/vp/app/create_video" style="color:#FFF;background-color: #5cb85c;border-radius:.3em;padding:0;padding:.5em;margin-top:.5em;margin-right:3em;">Add Video</a></li>
           <li><a href="/vp/app/">Videos</a></li>
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php $user = $this->ion_auth->user()->row(); ?><?php echo $user->email;?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="/vp/app/settings">Settings</a></li>
                <li><a href="#">Help</a></li>
                <li class="divider"></li>
                <li><a href="/vp/app/logout">Logout</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
 
 

