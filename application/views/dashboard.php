<?php $user = $this->ion_auth->user()->row(); ?>
<section>
 <div class="container">
   <div class="col-sm-12 col-md-12 main">
<div class="row">
    <div class="col-md-12">
       <h3 class="pageTitle"><?php echo $title;?></h3>
         <hr/>
        </div><!--/col-->
    </div><!--/row-->
      <?php if($results === false)  { ?>
            <p>You have no videos.</p>
      <?php } else { ?>
      <?php foreach($results as $data) : ?>
    <div class="row videoRow" id="videoBox-<?php echo $data->videoid; ?>">
        <div class="col-md-3">
        <a href="#" class="thumbnail">
          <img src="http://img.youtube.com/vi/<?php echo $data->url; ?>/0.jpg"/>
        </a>
        </div>
        <div class="col-md-9">
         <h4><?php echo $data->video_title; ?></h4>
         <p><span class="label label-default"><?php echo $data->datetime; ?></span></p>
            <div class="btn-group btn-group-sm videoButtons">
              <a href="/vp/app/video/id/<?php echo $data->videoid; ?>" class="btn btn-default">Edit</a>
              <a class="btn btn-default">Preview</a></dd>
              <a class="getEmbed-<?php echo $data->videoid; ?> btn btn-default">Embed</a>
              <a class="deleteEmbed-<?php echo $data->videoid; ?> btn btn-default" >Delete</a>
            </div>
           <p><?php echo $data->description; ?> </p>
            <div id="test-<?php echo $data->videoid; ?>"></div>
        </div>
      </div>
       <hr/>
  
   
<script>
$('.getEmbed-<?php echo $data->videoid; ?>').click(function(e){
   e.preventDefault();
   $.ajax({
	    type: "POST",
        url: "/vp/app/generate_embed_code",
		data: { 'videoid': '<?php echo $data->videoid; ?>' },
        success: function(result) {
        var html = jQuery('#test-<?php echo $data->videoid; ?>').html(result);
         

        },
    });
	 
});

$('.deleteEmbed-<?php echo $data->videoid; ?>').click(function(e){
var answer = confirm ("Are you sure you want to delete this video?");
if (answer)
  {
   e.preventDefault();
   $.ajax({
	    type: "POST",
        url: "/vp/app/delete_video",
		data: { 'videoid': '<?php echo $data->videoid; ?>' },
        success: function() {
		$( "#videoBox-<?php echo $data->videoid; ?>" ).fadeOut( "slow", function() {
		
		});
        },
    });
	
 	
  }
});

</script>
<?php endforeach; ?>	


<?php } ?>
<?php echo $links; ?>
</section>


