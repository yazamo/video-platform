<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Video Library</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
     <div class="collapse navbar-collapse">
      <div class="pull-right">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown register-dropdown">
          <a href="/login">Login to your account <!--<img class="userIcon" src="<?php //echo $base;?>images/userIcon.png"/>&nbsp;&nbsp;--> </a>
         </li>
       </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="mainSection">
<div class="container-fluid">
<div class="row">
     <div class="col-md-3 col-xs-3"></div>
 	 <div class="col-md-6 col-xs-6">
        <div class="text-center">
         <h3 class="formHeader">Please create an account to get started</h3>
            <?php if($message != false) { ;?>
		 <div class="alert alert-info alert-dismissable">
  		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		 <?php echo $message;?>
		 </div>
		 <?php }?>
        </div> 
	     <div class="registerWrap">
        <form action="/register" method="post" accept-charset="utf-8">
           <div class="row">
             <div class="col-md-6 col-xs-6">
                <div class="form-group">
             <label for="first_name">First Name:</label> 
			<input type="text" name="first_name" value="" id="first_name" class="form-control" required/>
 			 </div>
             </div>
             <div class="col-md-6 col-xs-6">
             <div class="form-group">
            <label for="last_name">Last Name:</label> 
            <input type="text" name="last_name" value="" id="last_name"  class="form-control" required/>  
             </div>
              </div>
           </div><!--row-->
            <div class="row">
             <div class="col-md-6 col-xs-6">
               <div class="form-group">
                <label for="company">Company Name:</label>
                <input type="text" name="company" value="" id="company" class="form-control" />  
                 </div>
             </div>
           
             <div class="col-md-6 col-xs-6">
                <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" name="email" value="" id="email"  class="form-control" required/>   
             </div>
             </div>
             </div>
              <div class="row">
             <div class="col-md-6 col-xs-6">
                <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" value="" id="password" class="form-control" required/>
                  </div>
              </div>
             <div class="col-md-6 col-xs-6">
                <div class="form-group">
             <label for="password_confirm">Confirm Password:</label>
             <input type="password" name="password_confirm" value="" id="password_confirm" class="form-control" required/>    
               </div>
             </div>
             
           </div><!--row-->
             <div class="text-center">
            <div class="text-center submitRow">
			<button class="btn btn-lg btn-primary greenBtn" type="submit">Register</button>
           </form>
           </div>
          </div>
           
 </div>
  
</div>
</div>
</section>

<div class="push"></div>
</div>



 
<footer style="background:#f9fcfd;border-top:1px solid #dddfdf;">
<div class="container-fluid">  
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div>
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<a href="http://www.yazamo.com/" target="_blank"><img src="<?php echo $base;?>images/copyright.jpg"/></a>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
 </footer> 
 
 
</div>
<div class="helpBtn">
<a href="#"></a>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
</body>
</html>