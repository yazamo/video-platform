<?php $user = $this->ion_auth->user()->row(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Account Settings</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
     <div class="collapse navbar-collapse">
      <div class="pull-right">
      <ul class="nav navbar-nav navbar-right">
       <li class="mainLink"><a href="/create_video/" class="current">Add Video</a></li>
       <li class="mainLink"><a href="/">Video Library</a></li>
       <li class="mainLink"><a href="/analytic_dashboard">Analytics</a></li>
       <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
        <li class="dropdown register-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <!--<img class="userIcon" src="<?php //echo $base;?>images/userIcon.png"/>&nbsp;&nbsp;--> <?php echo $user->first_name;?> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="/settings"><img src="<?php echo $base;?>images/settingsIcon.jpg"/>&nbsp;&nbsp;Settings</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/helpIcon.jpg"/>&nbsp;&nbsp;Help</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="/logout"><img src="<?php echo $base;?>images/logoutIcon.jpg"/>&nbsp;&nbsp;Logout</a></li>
          </ul>
        </li>
       
      </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="filterBar" style="height:60px;">
<div class="container-fluid">
<div class="col-md-8 col-xs-8">
<div class="text-left">
<h1 class="pageTitle">Add Video</h1>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
</section>
<section class="mainSection">
<div class="container-fluid">
<div class="row">
     <div class="col-md-3 col-xs-3"></div>
 	 <div class="col-md-6 col-xs-6">
        <div class="text-center">
         <h3 class="formHeader">Create a new video</h3>
        </div> 
	     <div class="registerWrap">
        <form action="/add_video" method="post" data-abide>
        <input type="hidden" value="<?php echo $user->id;?>" name="userID"/>
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <div class="form-group">
        <label>Video Title</label> 
        <input type="text" class="form-control" name="video_title" required/>
          </div><!--/form-group-->
          </div> <!--/col-md-12 col-xs-12-->
         </div><!--row-->
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <div class="form-group">
        <label>YouTube URL</label>
        <input type="text" class="form-control" name="url"  required/>
        </div><!--/form-group-->
         </div><!--/col-md-12 col-xs-12-->
           </div><!--/row-->
         <div class="row">
             <div class="col-md-8 col-xs-8">
                <div class="form-group webSelect">
                <?php if(empty($websites)) {?>
                 <label style="padding-top:1.5em;">You have no categories, please add one</label>
            <?php } else { ?>
             <label>Category</label>
        <select name="weburl">
            <?php foreach($websites as $data) : ?>
               <option value="<?php echo $data->websiteid; ?>"><?php echo $data->weburl; ?></option>
             <?php endforeach;?>
            </select>
            <?php } ?>
        </div><!--/form-group-->
        </div><!--/col-md-6 col-xs-6-->
          <div class="col-md-4 col-xs-4">
          <div class="form-group" style="padding-top:2em;">
          <!-- <label>Add new</label>-->
           <a data-toggle="modal" data-target="#addCatModal" class="btn btn-default addCat" class="form-control">New Category</a>
        <!--<input value="" type="text" class="form-control" name="weburlnew" placeholder="add new"/>-->
           </div><!--/form-group-->
        </div><!--/col-md-6 col-xs-6-->
        </div>
         <div class="text-center">
        <button type="submit" class="btn btn-default greenBtn">Create Video</button>
        </div><!--/text-center-->
        </form>
     

<?php
/*function randomize($len = false)
{
   $ints = array();
   $len = $len ? $len : rand(2,9);
   if($len > 9)
   {
      trigger_error('Maximum length should not exceed 9');
      return 0;
   }
   while(true)
   {
      $current = rand(0,9);
      if(!in_array($current,$ints))
      {
         $ints[] = $current;
      }
      if(count($ints) == $len)
      {
          return implode($ints);
      }
   }
}
$ran1 = randomize(4); //Numbers that are all unique with a random length.
 
$ran2 = randomize(7); //Numbers that are all unique with a length of 7
 
$sum = $ran1 * $ran2;

$sum_total = $sum + $user->id;

print ($sum_total);
*/
?>

 </div>
          </div>
          <div class="col-md-3 col-xs-3"></div>
 </div>
  
</div>
</div>
</section>


<div class="push"></div>
</div>
<footer style="background:#f9fcfd;border-top:1px solid #dddfdf;">
<div class="container-fluid">  
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div>
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<a href="http://www.yazamo.com/" target="_blank"><img src="<?php echo $base;?>images/copyright.jpg"/></a>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
 </footer> 
 
 
</div>
<div class="helpBtn">
<a href="#"></a>
</div>
<!-- add category -->
<div class="modal fade" id="addCatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Category</h4>
      </div>
      <div class="modal-body">
        <form id="addNewCat">
        <label>Category Name</label>
         <input type="text" class="form-control" value="" name="category_name"/>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save</button>
          </form>
      </div>
    </div>
  </div>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
 <script>
 $("#addNewCat").submit(function(e){
  e.preventDefault();
  var data = $('#addNewCat').serialize();
  $.ajax({
    type: "POST",
    url: "/add_category",
    data: data,
    success: function(){
	  $('#addCatModal').modal('hide')
      $('.webSelect') .load('/<?php echo uri_string();?> .webSelect', function() {
			/// can add another function here
	  });
    }
    });
});
</script>
 <script type="text/javascript">
 (function() {
   var s = document.createElement("script");
   s.type = "text/javascript";
   s.async = true;
   s.src = '//api.usersnap.com/load/b757bce7-66b1-48d4-afe7-f6d9a6663289.js';
   var x = document.getElementsByTagName('head')[0];
   x.appendChild(s);
 })();
</script>
</body>
</html>