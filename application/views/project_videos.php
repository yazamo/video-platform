  <ul class="list-group">
  <?php if (!empty($results)) { ?>
  <?php foreach($results as $video) :?>
      <li class="list-group-item">
        <h4 class="list-group-item-heading"><?php echo $video->video_title;?></h4>
        <p class="list-group-item-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis ipsum non nisi ullamcorper dictum. Ut luctus eleifend diam. Nulla erat lacus, rutrum id urna eget, congue tincidunt purus.</p>
        <br/> 
      <div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Move to Project <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
          <?php foreach($projects as $project) :?>
            <li><a class="moveProject" id="<?php echo $project->projectid ;?>" title="<?php echo $video->videoid;?>" href="javascript(void);"><?php echo $project->project_name ;?></a></li>
          <?php endforeach;?> 
          </ul>
        </div>
      </li> 
   <?php endforeach;?>   
   <?php } else { ?>
   <p>This project has no videos</p>
   <?php } ?>
 </ul>
 <script>
 $('.moveProject').click(function(e){
   var id = $(this).attr('id');
   var videoid = $(this).attr("title");
   e.preventDefault();
   $.ajax({
	    type: "POST",
        url: "/move_to_project",
		data: { 'projectid': id, 'videoid': videoid },
        success: function(result) {
        $('#videoResults').load('/projects #videoResults', function() {
	    });
       },
   });
});
</script>