<?php $user = $this->ion_auth->user()->row(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Video Library</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="#"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
    
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="filterBar" style="height:60px;">
<div class="container-fluid">
<div class="col-md-8 col-xs-8">
<div class="text-left">
<h1 class="pageTitle">Preview</h1>
</div>
</div>
 
</div>

</section>
<section class="mainSection">
<div class="container-fluid">
<div class="row">
<div class="col-12-lg">
<div id="wd_id" style="max-width:<?php echo $result[0]->width; ?>px;margin:20px auto;"></div>  <script type="text/javascript" src="https://dev.bettercapture.com/assets/js/yzplayer.js"></script>  <script type="text/javascript">  init_widget(<?php echo $result[0]->userid; ?>,<?php echo $result[0]->videoid; ?>,<?php echo $api_type[0]->api_type; ?>,<?php echo $result[0]->width; ?>,<?php echo $result[0]->height; ?>) </script>
</div>
</div>
</div>
<div class="push"></div>
</div>
</div>
</section>
<footer style="background:#f9fcfd;border-top:1px solid #dddfdf;">
<div class="container-fluid">  
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div>
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<img src="<?php echo $base;?>images/copyright.jpg"/>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
 </footer> 
 </div>
<div class="helpBtn">
<a href="#"></a>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
 </body>
</html>