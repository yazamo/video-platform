<div class="api">
 <div class="row">
  <div class="col-md-12">
      <p>Please select an email provider below</p>
      <hr/>
   </div>
 </div>
 <div style="background:#FFF;border-radius:4px;border: 1px solid #dedfe0;padding-bottom:1em;">    
 <div class="row">
    <div class="col-md-6 text-center logoBlock">
    <img src="/assets/images/crm_logos/is.jpg" class="is"/>
   <p> <a id="1" class="btn btn-default" href="https://signin.infusionsoft.com/app/oauth/authorize?response_type=code&client_id=mmqtwx75tm7n4cce2wzqpaaa&redirect_uri=https%3A%2F%2Fdev.bettercapture.com%2Fadd_infusionsoft&scope=full">Connect</a></p>
    </div><!--/col-md-4-->
    <div class="col-md-6 text-center logoBlock">
     <img src="/assets/images/crm_logos/mc.jpg" class="mc"/>
    <p><a id="2" class="btn btn-default" href="https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=898401133622&redirect_uri=http%3A%2F%2Fdev.bettercapture.com%2Fadd_mailchimp">Connect</a></p>
    </div><!--/col-md-4-->
  </div>
   <div class="row">
    <div class="col-md-6 text-center logoBlock">
     <img src="/assets/images/crm_logos/cm.jpg" class="cm"/>
    <p><a id="4" class="btn btn-default" href="https://dev.bettercapture.com/campaignmonitor">Connect</a></p>
    </div><!--/col-md-4-->
    <div class="col-md-6 text-center logoBlock">
     <img src="/assets/images/crm_logos/cc.jpg" class="cc"/>
    <p><a id="3" class="btn btn-default" href="/constantcontact">Connect</a></p>
    </div><!--/col-md-4-->
 </div>
 </div>
 
</div>         
         