<?php $user = $this->ion_auth->user()->row(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Video Library</title>
<?php $base = '/vp/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="#"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
     <div class="collapse navbar-collapse">
      <div class="pull-right">
      <ul class="nav navbar-nav navbar-right">
       <li class="mainLink"><a href="#">Add Video</a></li>
       <li class="mainLink"><a href="#" class="current">Video Library</a></li>
       <li class="mainLink"><a href="#">Analytics</a></li>
       <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
        <li class="dropdown register-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <!--<img class="userIcon" src="<?php //echo $base;?>images/userIcon.png"/>&nbsp;&nbsp;--> Jack Howard <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><img src="<?php echo $base;?>images/settingsIcon.jpg"/>&nbsp;&nbsp;Settings</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/helpIcon.jpg"/>&nbsp;&nbsp;Help</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/logoutIcon.jpg"/>&nbsp;&nbsp;Logout</a></li>
          </ul>
        </li>
       
      </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="filterBar">
<div class="container-fluid">
<div class="col-md-8 col-xs-8">
<div class="text-left">
<h1 class="pageTitle">Your Videos :<small><?php if(empty($websingle[0]->weburl)) { ?> All <?php } else { echo $websingle[0]->weburl; }?></small></h1>
</div>
</div>
<div class="col-md-4 col-xs-4">
<form class="form-horizontal filterForm" role="form" id="filterVideos" action="/vp/app/videos" method="post">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 col-md-4 control-label">Filter Videos</label>
    <div class="col-sm-8 col-md-8">
     <select name="checkwebID" id="checkwebID" onchange='this.form.submit()'>
          <option value="">Filter by Category</option> 
             <option value="0">ALL</option>
             <?php foreach($websites as $data) : ?>
               <option value="<?php echo $data->websiteid; ?>"><?php echo $data->weburl; ?></option>
             <?php endforeach;?>
            </select>
    </div>
  </div>
 </form>
</div>
</div>
</section>







 <section>
 <div class="container">
   <div class="col-sm-12 col-md-12 main">
<div class="row">
    <div class="col-md-12">
      <form class="pull-right" id="filterVideos" action="/vp/app/videos" method="post">
          <label>Filter Website : </label>
          <select name="checkwebID" id="checkwebID" onchange='this.form.submit()'>
          <option value="">select</option> 
             <option value="0">ALL</option>
             <?php foreach($websites as $data) : ?>
               <option value="<?php echo $data->websiteid; ?>"><?php echo $data->weburl; ?></option>
             <?php endforeach;?>
            </select>
      </form> 
       <h3 class="pageTitle"><?php echo $title;?>: <small><?php if(empty($websingle[0]->weburl)) { ?> All <?php } else { echo $websingle[0]->weburl; }?></small></h3>
    	<hr/>
        </div><!--/col-->
    </div><!--/row-->
      <?php if($results === false)  { ?>
            <p>You have no videos.</p>
      <?php } else { ?>
      <?php foreach($results as $data) : ?>
    <div class="row videoRow" id="videoBox-<?php echo $data->videoid; ?>">
        <div class="col-md-3">
        <a href="#" class="thumbnail">
          <img src="http://img.youtube.com/vi/<?php echo $data->url; ?>/0.jpg"/>
        </a>
        </div>
        <div class="col-md-9">
         <h4><?php echo $data->video_title; ?></h4>
         <p><span class="label label-default"><?php echo $data->datetime; ?></span></p>
            <div class="btn-group btn-group-sm videoButtons">
              <a href="/vp/app/video/id/<?php echo $data->videoid; ?>" class="btn btn-default">Edit</a>
              <a id="<?php echo $data->videoid; ?>" class="previewVideo btn btn-default" data-toggle="modal" data-target="#previewModal">Preview</a></dd>
              <a id="<?php echo $data->videoid; ?>" class="getEmbed btn btn-default"  data-toggle="modal" data-target="#embedModal">Embed</a>
              <a id="<?php echo $data->videoid; ?>" class="deleteEmbed btn btn-default">Delete</a>
            </div>
           <p><?php echo $data->description; ?> </p>
          
        </div>
      </div>
       <hr/>
<?php endforeach; ?>	
<?php } ?>
<?php echo $links; ?>
</section>



<!-- Embed Modal -->
<div class="modal fade" id="embedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Embed Code</h4>
      </div>
      <div id="embedCode" class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Copy</button>
      </div>
    </div>
  </div>
</div>


<!-- Preview Modal -->
<div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Video Preview</h4>
      </div>
      <div id="previewVid" class="modal-body">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     
      </div>
    </div>
  </div>
</div>

<script>
/* Embed code AJAX Call */
$('.getEmbed').click(function(e){
   var id = $(this).attr('id');
   e.preventDefault();
   $.ajax({
	    type: "POST",
        url: "/vp/app/generate_embed_code",
		data: { 'videoid': id },
        success: function(result) {
        var html = jQuery('#embedCode').html(result);
      },
   });
});


/* Delete Video AJAX Call */
$('.deleteEmbed').click(function(e){
	var answer = confirm ("Are you sure you want to delete this video?");
	var id = $(this).attr('id');
	if (answer)
	  {
	   e.preventDefault();
	   $.ajax({
			type: "POST",
			url: "/vp/app/delete_video",
			data: { 'videoid': id  },
			success: function() {
			$( "#videoBox-"+id ).fadeOut( "slow", function() {
			
			});
			},
		});
      }
});


/* Embed code AJAX Call */
$('.previewVideo').click(function(e){
   var id = $(this).attr('id');
   e.preventDefault();
   $.ajax({
	    type: "POST",
        url: "/vp/app/preview_video",
		data: { 'videoid': id },
        success: function(result) {
        var html = jQuery('#previewVid').html(result);
      },
   });
});
</script>


