<?php $user = $this->ion_auth->user()->row(); ?>
<section>
 <div class="container">
   <div class="col-sm-12 col-md-12 main">
<div class="row">
    <div class="col-md-12">
        <h3 class="pageTitle"><?php echo $title;?></h3>
        <?php /*<p><?php var_dump($result[0]->video_title);?><p>*/ ?>
    </div><!--/col-md-2-->
 </div>
 <hr style="margin-top:0;"/>
 <div class="row">
   <div class="col-md-6">
<?php // echo $alert;?>
<div class="row">
    <div class="col-md-12"> 
        <div class="btn-group pull-right">
          <a class="btn btn-default" href="#options" data-toggle="tab">Options</a>
          <a class="btn btn-default" href="#callToAction" data-toggle="tab">Call to Action</a>
          <a class="btn btn-default" href="#optIn" data-toggle="tab">Opt In</a>
          <a class="btn btn-default" href="#apps" data-toggle="tab">Integration</a>
          <a class="btn btn-default" href="#embed" data-toggle="tab">Embed</a>
        </div><!--/btn-group-->
    </div><!--/col-md-10-->
</div><!--/row-->
 <div class="row">
     <div class="col-md-12">
       <div class="tab-content">
   
  <div class="tab-pane active" id="options">
 <form action="" method="post">
 <input type="hidden" value="<?php echo $result[0]->videoid; ?>" name="videoID"/>
 <input type="hidden" value="<?php echo $user->id;?>" name="userID"/>
 <div class="form-group">
    <label for="video_title">Video Title</label>
    <input type="text" name="video_title" value="<?php echo $result[0]->video_title; ?>" class="form-control" />
 </div><!--/form-group-->
 <div class="row">
 <div class="col-md-9">
  <div class="form-group">
    <label for="url">Youtube URL</label>
 	<input type="text" name="url" value="http://www.youtube.com/watch?v=<?php echo $result[0]->url; ?>"class="form-control"/>
  </div><!--/form-group-->
  </div>
  <div class="col-md-3">
  <div class="form-group">
 <label>Player Controls</label>
   <br/>
 <?php if($result[0]->timeline == '0') { ?>
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
      <input type="radio" name="timeline" value="1" id="timelineOn"> On
      </label>
      <label class="btn btn-danger active">
    <input type="radio" name="timeline" value="0" id="timelineOff" checked="checked"> Off
    </label>
     </div><!--/btn-group-->
<?php	 } else { ?>
<div class="btn-group" data-toggle="buttons">
   <label class="btn btn-success active">
   <input type="radio" name="timeline" value="1" id="timelineOn" checked="checked"> On
   </label>
   <label class="btn btn-default">
    <input type="radio" name="timeline" value="0" id="timelineOff"> Off
    </label>
     </div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->
</div>
</div>
<div class="row">
 <div class="col-md-6">
    <div class="row">
  <div class="col-md-6">
  <div class="form-group">
    <label for="width">Width</label>
      	   <div class="input-group">
           <input type="text" name="width" value="<?php echo $result[0]->width; ?>" class="form-control"/>
            <span class="input-group-addon">px</span>
            </div><!--/input-group-->
    </div><!--/form-group-->
    </div><!--/col-md-6-->
    <div class="col-md-6">
  <div class="form-group">
    <label for="height">Height</label>  
         <div class="input-group">
         <input type="text" name="height" value="<?php echo $result[0]->height; ?>" class="form-control"/>
         <span class="input-group-addon">px</span>
         </div><!--/input-group-->
 </div><!--/form-group-->
 </div><!--/col-md-6-->
</div><!--/row-->
 
 </div><!--/col-md-4-->
 <div class="col-md-3">
   <div class="form-group">
        <label for="height">Autoplay</label>
      <br/>
        <?php if($result[0]->autoplay == '0') { ?>
       <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="autoplay" value="1" id="autoplayOn"> On
      </label>
      <label class="btn btn-danger active">
        <input type="radio" name="autoplay" value="0" id="autoplayOff" checked="checked"> Off
      </label> 
     </div><!--/btn-group-->
     <?php	 } else { ?>  
       <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-success active">
        <input type="radio" name="autoplay" value="1" id="autoplayOn" checked="checked"> On
        </label>
        <label class="btn btn-default">
        <input type="radio" name="autoplay" value="0" id="autoplayOff"> Off
        </div><!--/btn-group-->
     <?php } ?>
    </div><!--/form-group-->
   
 </div><!--/col-md-3-->
 <div class="col-md-3">
  <div class="form-group">  
   <label>Enable HD</label>
   <br/>
 <?php if($result[0]->hd == '0') { ?>
  <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-default">
    <input type="radio" name="hd" value="1" id="hdOn"> On
    </label>
     <label class="btn btn-danger active">
    <input type="radio" name="hd" value="0" id="hdOff" checked="checked"> Off
    </label>
    </div><!--/btn-group-->
<?php	 } else { ?>
  <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-default active">
    <input type="radio" name="hd" value="1" id="hdOn" checked="checked"> On
    </label>
     <label class="btn btn-default">
    <input type="radio" name="hd" value="0" id="hdOff"> Off
    </label>
  </div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->
</div><!--/col-md-3-->
</div><!--/row-->
</div><!--/tab-->
<div class="tab-pane" id="callToAction">  
<div class="form-group">    
   <label>Call to Action</label>
   <br/>
 <?php if($result[0]->cta == '0') { ?>
    <div class="btn-group" data-toggle="buttons">
     <label class="btn btn-default">
     <input type="radio" name="cta" value="1" id="ctaOn"> On
     </label>
     <label class="btn btn-default active">
     <input type="radio" name="cta" value="0" id="ctaOff" checked="checked"> Off
     </label>
    </div><!--/btn-group-->
<?php	 } else { ?>
 <div class="btn-group" data-toggle="buttons">
     <label class="btn btn-default active">
     <input type="radio" name="cta" value="1" id="ctaOn" checked="checked"> On
     </label>
     <label class="btn btn-default">
     <input type="radio" name="cta" value="0" id="ctaOff"> Off
     </label>
   </div><!--/btn-group-->
<?php } ?>


</div><!--/form-group-->
<div class="form-group"> 
  <label>Call to Action Start</label>
  <br/>
<?php if($result[0]->ctatime == '0') { ?>
   <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-default">
    <input type="radio" name="ctatime" value="1" id="ctatimeOn"> Start
    </label>
    <label class="btn btn-default active">
    <input type="radio" name="ctatime" value="0" id="ctatimeOff" checked="checked"> End
    </label>
    </div><!--/btn-group-->
<?php } else { ?>
   <div class="btn-group" data-toggle="buttons">
   <label class="btn btn-default active">
    <input type="radio" name="ctatime" value="1" id="ctatimeOn" checked="checked"> Start
    </label>
    <label class="btn btn-default">
    <input type="radio" name="ctatime" value="0" id="ctatimeOff"> End
    </label>
   </div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->


<div class="form-group">    
   <label>Time for CTA to appear</label>
<br/>
<?php
 
$vidID="lHvF_ANqAic";
$url = "http://gdata.youtube.com/feeds/api/videos/". $vidID;
$doc = new DOMDocument;
$doc->load($url);
$title = $doc->getElementsByTagName("title")->item(0)->nodeValue;
$duration = $doc->getElementsByTagName('duration')->item(0)->getAttribute('seconds');
?>
Duration: <?php echo gmdate("H:i:s", $duration );?> <br />

</div>
</div><!--/tab-->
<div class="tab-pane" id="optIn"> 
<div class="form-group">   
<label>Opt in</label>
<br/>
 <?php if($result[0]->opt == '0') { ?>
  <div class="btn-group" data-toggle="buttons">
   <label class="btn btn-default">
  <input type="radio" name="opt" value="1" id="optOn"> On
  </label>
  <label class="btn btn-default active">
  <input type="radio" name="opt" value="0" id="optOff" checked="checked"> Off
  </label>
  </div><!--/btn-group-->
<?php	 } else { ?>
<div class="btn-group" data-toggle="buttons">
   <label class="btn btn-default active">
   <input type="radio" name="opt" value="1" id="optOn" checked="checked"> On
   </label>
    <label class="btn btn-default">
    <input type="radio" name="opt" value="0" id="optOff"> Off
   <label>
</div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->
</div><!--/tab-->   
 
 
 <div class="tab-pane" id="apps"> 
 <div class="form-group">    
 <?php
 /* Check if IS List is set then display if true */
 if(isset($tags)) { ?>
   <label>Infusionsoft</label>
   <br/>
 
 <select id="emailSelect" name="input_field[]">
 <option>select list</option>
 <?php  foreach($tags as $data) : ?>
 <option  value="<?php echo $data['Id']; ?>"><?php echo $data['GroupName']; ?></option>
   <?php endforeach; ?>	
   <input id="listName" name="input_field[]" type="hidden" value="" />
 </select>
 
<?php  } ?>

<?php
 /* Check if MC List is set then display if true */
 if(isset($getUserList)) { ?>
 <label>MailChimp</label>
  <br/>
 <select id="emailSelect" name="input_field[]">
 <option>select list</option>
 <?php  foreach ($getUserList['data'] as $listName => $data) : ?>
	 
<option value="<?php echo $data['id'];?>"> <?php echo $data['name'];?> </option>

 <?php endforeach; ?>	
<input id="listName" name="input_field[]" type="hidden" value="" />
</select>
 <?php  } ?>
 </div>
 <script type='text/javascript'>
 /* This code grabs the list name and sets the value on hidde field for the php array */
$(function() {
    $('#emailSelect').change(function() {
      
        var x =  $('#emailSelect option:selected').text();
       
        $('#listName').val(x);
    });
});
</script>
 
 </div><!--/tab--> 
 </div><!--/tab-content-->
 </div><!--/col-md-12-->
 </div> <!--/row-->

 <div class="row">
<div class="col-md-8 col-md-offset-4">
<div class="btn-group btn-group-lg text-center" style="padding-top:2em;">
  <button type="submit" value="Save Changes" name="submit" class="btn btn-default">Save</button>
  <a href="/vp/app/" class="btn btn-default">Back to Videos</a>
  <button type="button" data-toggle="modal" data-target="#myModal-<?php echo $result[0]->videoid; ?>" id="modalLink-<?php echo $result[0]->videoid; ?>" class="btn btn-default">Preview</button>
</div><!--/btn-group btn-group-lg-->
</div>
</div>
 </form>
</div><!--/col-md-6-->
   
   
<div class="col-md-6">
    <div class="panel panel-default">
    <div class="panel-heading">
    	<h3 class="panel-title">Preview</h3>
    </div><!--/panel-heading-->
    <div class="panel-body">
    	<div id="player" class="ytplayer" style="height:<?php $result[0]->height; ?>px;width:<?php echo $result[0]->width; ?>px;overflow:hidden;"></div><!--player-->
   <!--<button id="pause">Click to Pause Video</button>-->
    </div><!--/panel-body-->
    </div><!--/panel-->
</div><!--/col-md-6-->


</div><!--/row-->
 </div><!--/col-md-12 main-->
 </div><!--/container-->
</section>

 
 

<script>
 
 

   
 
 
	
	// 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	  // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
 

      
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
		height: '<?php echo $result[0]->height; ?>',
        width: '<?php echo $result[0]->width; ?>',
        videoId: '<?php echo $result[0]->url; ?>',
		playerVars: { 'autoplay': <?php echo $result[0]->autoplay; ?>, 'controls': <?php echo $result[0]->timeline; ?>, 'hd': <?php echo $result[0]->hd; ?>, 'rel': 0, 'showinfo': 0,'enablejsapi': 1},
		
		  events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange,
			 'onStateChange': stateChange
          }
        });
    }

   // when video starts
	function onPlayerReady(event) {
	<?php if($result[0]->ctatime === '1') { ?>
	     alert('Cta Start');
	<?php } ?>
	
	
	<?php if($result[0]->autoplay === '1') { ?>
	     event.target.playVideo();
    <?php } ?>
	}
 
    // when video ends
        function onPlayerStateChange(event) {        
            if(event.data === 0) {         
			
			<?php  //if ($r->ctatime === '0'){ ?> 
                alert('Cta End');
			<?php // } ?>   
            }
      }	
	  
	  
	  
	  
	
 
			
			
			
var timestamp = 20; /* your timestamp */

    var timer;

    function timestamp_reached() {
       //alert('timestamp reached');
	    player.pauseVideo();
    }

    function timestamp_callback() {
        clearTimeout(timer);
            
        current_time = player.getCurrentTime();
            //alert(current_time);
        remaining_time = timestamp - current_time;
            //alert(remaining_time);
        if (remaining_time > 0) {
            timer = setTimeout(timestamp_reached, remaining_time * 1000);
        }    
    }
    
    function stateChange(evt) {
            //alert(evt.data);
        if (evt.data == YT.PlayerState.PLAYING) {
            timestamp_callback();
        }
    };
	
	
	
	document.getElementById('pause').onclick = function() {
        player.pauseVideo();
    };
	
	
	document.getElementById('resume').onclick = function() {
        player.playVideo();
    };

</script>
