<?php $user = $this->ion_auth->user()->row(); ?>
<section>
 <div class="container">
   <div class="col-sm-12 col-md-12 main">
<div class="row">
    <div class="col-md-12">
       <h3 class="pageTitle"><?php echo $title;?></h3>
         <hr/>
        </div>
    </div>
    <div class="row">
     <div class="col-md-6">
        <form action="/vp/app/add_video" method="post" data-abide>
        <input type="hidden" value="<?php echo $user->id;?>" name="userID"/>
        <label>Video Title</label>
        <div class="input-group-md">
        <input type="text" class="form-control" name="video_title" required/>
        </div>
         <br/>
        <label>YouTube URL</label>
        <div class="input-group-md">
        <input type="text" class="form-control" name="url"  required/>
        </div>
        <br/>
        <label>Website</label>
        <div class="input-group-md">
        New: <input value="" type="text" class="form-control" name="weburlnew"/>
        </div>
         <label></label>
          <div class="input-group-md">
        Exisiting:
        
        <select name="weburl">
            <?php foreach($websites as $data) : ?>
               <option value="<?php echo $data->websiteid; ?>"><?php echo $data->weburl; ?></option>
             <?php endforeach;?>
            </select>
        </div>
        <br/>
        <div class="input-group-md">
        <input type="submit" value="Create Video" class="btn btn-lg btn-primary btn-block"/>
        </div>
        </form>
     </div>
     <div class="col-md-6">
     
     </div>
    </div>

<?php
/*function randomize($len = false)
{
   $ints = array();
   $len = $len ? $len : rand(2,9);
   if($len > 9)
   {
      trigger_error('Maximum length should not exceed 9');
      return 0;
   }
   while(true)
   {
      $current = rand(0,9);
      if(!in_array($current,$ints))
      {
         $ints[] = $current;
      }
      if(count($ints) == $len)
      {
          return implode($ints);
      }
   }
}
$ran1 = randomize(4); //Numbers that are all unique with a random length.
 
$ran2 = randomize(7); //Numbers that are all unique with a length of 7
 
$sum = $ran1 * $ran2;

$sum_total = $sum + $user->id;

print ($sum_total);
*/
?>

</div>


</div>
</section>