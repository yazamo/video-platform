<?php $user = $this->ion_auth->user()->row(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Account Settings</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
     <div class="collapse navbar-collapse">
      <div class="pull-right">
      <ul class="nav navbar-nav navbar-right">
       <li class="mainLink"><a href="/create_video">Add Video</a></li>
       <li class="mainLink"><a href="/videos">Video Library</a></li>
       <li class="mainLink"><a href="/analytic_dashboard">Analytics</a></li>
       <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
        <li class="dropdown register-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <!--<img class="userIcon" src="<?php //echo $base;?>images/userIcon.png"/>&nbsp;&nbsp;--> <?php echo $user->first_name;?> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="/settings"><img src="<?php echo $base;?>images/settingsIcon.jpg"/>&nbsp;&nbsp;Settings</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/helpIcon.jpg"/>&nbsp;&nbsp;Help</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="/logout"><img src="<?php echo $base;?>images/logoutIcon.jpg"/>&nbsp;&nbsp;Logout</a></li>
          </ul>
        </li>
       
      </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="filterBar" style="height:60px;">
<div class="container-fluid">
<div class="col-md-8 col-xs-8">
<div class="text-left">
<h1 class="pageTitle">Account Settings</h1>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
</section>
<section class="mainSection">
<div class="container-fluid">
<div class="row">
<div class="col-md-2 col-xs-2 col-md-offset-2" style="padding-left:0;padding-right:0;background:#f0f3f4;">
<!-- Nav tabs -->
<ul class="nav nav-pills nav-stacked settingsNav">
  <li class="active"><a href="#account" data-toggle="tab" style="border-top:none;">Account Settings</a></li>
  <li><a href="#integration" data-toggle="tab"  style="border-bottom:1px solid #cdd0d1;">Email Integration</a></li>
  <!--<li><a href="#messages" data-toggle="tab" style="border-bottom:1px solid #cdd0d1;">Subscription and Payment</a></li> -->
<li style="width:100%;height:50%;border-right:1px solid #CCC;"></li>
</ul>

</div>
<div class="col-md-6 col-xs-6" style="padding-left:0;">
<!-- Tab panes -->
<div class="tab-content settingsContent">
  <div class="tab-pane active" id="account">
   <div class="row">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <p><?php echo $user->first_name;?> <?php echo $user->last_name;?></p>
          </div>
    </div>
    <div class="col-md-4 col-xs-4">
       <div class="form-group">
            <label for="exampleInputEmail1">Company</label>
           <p><?php echo $user->company;?></p>
          </div>
    </div>
    <div class="col-md-4 col-xs-4">
       <div class="form-group">
            <label for="exampleInputEmail1">Password</label>
           <p><a href="/change_password">Change</a></p>
          </div>
    </div>
   </div><!--/row-->
   <hr/>
    <div class="row">
 
 
</div><!--/row-->
  
 
  </div>
  <div class="tab-pane" id="integration">
 
        <?php echo $api_type;?>

</div>
</section>


<div class="push"></div>
</div>
<footer style="background:#f9fcfd;border-top:1px solid #dddfdf;">
<div class="container-fluid">  
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div>
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<a href="http://www.yazamo.com/" target="_blank"><img src="<?php echo $base;?>images/copyright.jpg"/></a>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
 </footer> 
 
 
</div>
<div class="helpBtn">
<a href="#"></a>
</div>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
 <script>
$( "button" ).click(function() {
  var id = $(this).attr('id');
  $( ".test").hide();
  $( ".api").hide();	
  $( "#apiForm"+id ).show();
  
});

$( ".cancel" ).click(function() {
	var id = $(this).attr('id');
	$( "#apiForm"+id ).hide();
	$( ".api").show();
});
</script>
</body>
</html>







<?php
/*require_once("/src/isdk.php");
$app = new iSDK;
// Test Connnection
if ($app->cfgCon("et174", "99b4fb9c85e5f2e87acb2442d0edfba5"))
{


$returnFields = array('Id','GroupName');
$query = array('Id' => '%');
$tags = $app->dsQuery("ContactGroup",10,0,$query,$returnFields);

//var_dump($tags);

}
else
{
echo "Not Connected...";
*/
?>


 