<?php $user = $this->ion_auth->user()->row(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Video Library</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="#"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
     <div class="collapse navbar-collapse">
      <div class="pull-right">
      <ul class="nav navbar-nav navbar-right">
       <li class="mainLink"><a href="/create_video">Add Video</a></li>
       <li class="mainLink"><a href="/">Video Library</a></li>
       <li class="mainLink"><a href="/projects" class="current">Projects</a></li>
       <li class="mainLink"><a href="#">Analytics</a></li>
       <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
        <li class="dropdown register-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <!--<img class="userIcon" src="<?php //echo $base;?>images/userIcon.png"/>&nbsp;&nbsp;--> <?php echo $user->first_name;?> <b class="caret"></b></a>
           <ul class="dropdown-menu">
            <li><a href="#"><img src="<?php echo $base;?>images/settingsIcon.jpg"/>&nbsp;&nbsp;Settings</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/helpIcon.jpg"/>&nbsp;&nbsp;Help</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/logoutIcon.jpg"/>&nbsp;&nbsp;Logout</a></li>
          </ul>
        </li>
       </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="filterBar">
<div class="container-fluid">
<div class="col-md-8 col-xs-8">
<div class="text-left">
<h1 class="pageTitle">Your Projects</h1>
</div>
</div>
<div class="col-md-4 col-xs-4" style="height:60px;">
 
</div>
</div>
</section>
<section class="mainSection">
<div class="container-fluid">
    <div class="row">
    <div class="col-md-6 col-xs-6">
    <div id="test"></div>
    <div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Select Project</div>
  <div class="panel-body">
   <form id="addProject">
     <div class="input-group">
     <input type="text" id="project_name" name="project_name" class="form-control" placeholder="Project Name" required>
      <span class="input-group-btn">
        <input type="submit" class="btn btn-default" type="button" value="New Project"/>
      </span>
      </div><!-- /input-group -->
       </form>
  </div>
    <ul class="list-group projects">
    <?php foreach ($results as $project) : ?>
    <li class="list-group-item" id="projectList-<?php echo $project->projectid ;?>"><?php echo $project->project_name ;?>
   <?php if($project->project_name != 'uncategorized') { ?>
    <a id="<?php echo $project->projectid ;?>" href="javascript(void);" class="pull-right deleteProject">&nbsp;&nbsp;&nbsp;&nbsp;delete</a>
    <a href="" class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;edit</a>
     <?php } ?>
    <a  id="<?php echo $project->projectid ;?>" href="javascript(void);" class="pull-right getVideos">videos</a></li>
    <?php endforeach;?>
    </ul>
    </div>
    </div> 
    <div class="col-md-6 col-xs-6">
     <div class="panel panel-default">
     <div class="panel-heading">Project Videos</div>
  <div class="panel-body" id="videoResults">
     
      
       </div>
     </div>
     </div>
     
</div>
<div class="push"></div>
</div>
</div>
</section>
<footer style="background:#f9fcfd;border-top:1px solid #dddfdf;">
<div class="container-fluid">  
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div>
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<img src="<?php echo $base;?>images/copyright.jpg"/>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
 </footer> 
 
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
 <script>
 /* Embed code AJAX Call */
$('.getVideos').click(function(e){
   var id = $(this).attr('id');
   e.preventDefault();
   $.ajax({
	    type: "POST",
        url: "/get_project_videos",
		data: { 'projectid': id },
        success: function(result) {
        var html = jQuery('#videoResults').html(result);
       },
   });
});


/* Delete Video AJAX Call */
$('.deleteProject').click(function(e){
	var answer = confirm ("Are you sure you want to delete this project?");
	var id = $(this).attr('id');
	if (answer)
	  {
	   e.preventDefault();
	   $.ajax({
			type: "POST",
			url: "/delete_project",
			data: { 'projectid': id  },
			success: function() {
			 $( "#projectList-"+id ).fadeOut( "slow", function() { 
			  });
			$('#videoResults').load('/text #text', function() {
			/// can add another function here
			}); 
		},
		});
      }
});

$("#addProject").submit(function(e){
  e.preventDefault();
  var data = $('#addProject').serialize();
  $.ajax({
    type: "POST",
    url: "/add_project",
    data: data,
    success: function(){
      $('.projects').load('/projects .projects', function() {
	  }); 
	 $('#videoResults').load('/text #text', function() {
	 }); 
    }
    });
});




</script>
</body>
</html>