
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Login</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<div class="wrap">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <div class="pull-right register">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/register"><img class="userIcon" src="<?php echo $base;?>images/userIcon.png"/>&nbsp;&nbsp; Register Now</a></li>
        <!--<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>-->
      </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav><!--/-->
</header><!--/-->
<section class="mainSection">
<div class="container-fluid">
  <div class="row">
     <div class="col-md-4 col-xs-4"></div>
 	 <div class="col-md-4 col-xs-4">
         <div class="formCol">
<h3 class="formHeader"><?php echo lang('change_password_heading');?></h3>

<div id="infoMessage"><?php echo $message;?></div>
<?php ini_set('display_errors', 1);?>
<div class="formWrap" style="height:350px;"> 
<form action="/change_password" method="post" accept-charset="utf-8" class="form-signin">
  <div class="form-group">
     
            <label for="old_password">Old Password:</label>  
            <input type="password" name="old" value="" id="old" class="form-control" />      
</div>
<div class="form-group">
       
            <label for="new_password">New Password (at least 8 characters long):</label>  
            <input type="password" name="new" value="" id="new" pattern="^.{8}.*$" class="form-control" />       

 </div>
 <div class="form-group">    
            <label for="new_password_confirm">Confirm New Password:</label>  
            <input type="password" name="new_confirm" value="" id="new_confirm" pattern="^.{8}.*$" class="form-control" />      
</div>
       <?php echo form_input($user_id);?>
      <div style="text-align:center;">
             <input type="submit" name="submit" value="Change"  class="btn btn-default greenBtn"/>
               </div><!--/-->

</form>

</div><!--/-->
          </div><!--/-->
     </div><!--/-->
  	 <div class="col-md-4 col-xs-4"></div><!--/-->
  </div><!--/-->
</div><!--/-->
</section><!--/-->
<div class="navbar navbar-fixed-bottom">
<footer>
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div><!--/-->
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<a href="http://www.yazamo.com/" target="_blank"><img src="<?php echo $base;?>images/copyright.jpg"/></a>
</div><!--/-->
</div><!--/-->
<div class="col-md-4 col-xs-4">
</div><!--/-->
</footer><!--/-->
</div><!--/-->
</div><!--/-->
<div class="helpBtn">
<a href="#"></a>
</div><!--/-->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
</body>
</html>
 
