<?php $user = $this->ion_auth->user()->row(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Account Settings</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $base;?>css/artisto.css">

</head>
<body class="betterCapture">
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
     <div class="collapse navbar-collapse">
      <div class="pull-right">
      <ul class="nav navbar-nav navbar-right">
       <li class="mainLink"><a href="/create_video/">Add Video</a></li>
       <li class="mainLink"><a href="/">Video Library</a></li>
       <li class="mainLink"><a href="#"  class="current">Analytics</a></li>
       <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
        <li class="dropdown register-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <!--<img class="userIcon" src="<?php //echo $base;?>images/userIcon.png"/>&nbsp;&nbsp;--> <?php echo $user->first_name;?> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="/settings"><img src="<?php echo $base;?>images/settingsIcon.jpg"/>&nbsp;&nbsp;Settings</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/helpIcon.jpg"/>&nbsp;&nbsp;Help</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="/logout"><img src="<?php echo $base;?>images/logoutIcon.jpg"/>&nbsp;&nbsp;Logout</a></li>
          </ul>
        </li>
       
      </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="filterBar" style="height:70px;">
<div class="container-fluid">
<div class="col-md-8 col-xs-8">
<div class="text-left">
<h1 class="pageTitle">Analytics</h1>
</div>
</div>
<div class="col-md-4 col-xs-4">
 </div>
</div>
</section>
<section class="mainSection">
<div class="container-fluid">
<div class="row">
<div class="analtyics_wrap">
     <div class="col-md-12 col-xs-12">
     <div class="row selectFilterRow">
     <div class="col-md-10">
     <div class="row">
      <div class="col-md-3 col-xs-3">
       <?php print_r( $chartData);?>
      <h2>General Impressions</h2>
      </div>
      <div class="col-md-3 col-xs-3">
       <form  name="checkwebID" id="checkwebID"  action="/analytics" method="post" class="form-horizontal" >
        <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Video</label>
    <div class="col-sm-10">
         <select name="checkwebID" id="checkwebID" onchange="window.open(this.options[this.selectedIndex].value,'_top')">
          <option value=""><?php  echo $video[0]->video_title;?></option> 
             <?php foreach($videos as $data) : ?>
               <option value="/analytics/id/<?php echo $data->videoid; ?>"><?php echo $data->video_title; ?></option>
             <?php endforeach;?>
            </select>
      </div>
   </div>
    </form> 
    
   </div>
   <div class="col-md-3 col-xs-3">
     <?php
	    $get = $this->uri->ruri_to_assoc();
		$videoID = $get['id'];
		 
		?>
    <?php /* <form id="dateSelect" action="/analytics/id/<?php echo $videos[0]->videoid; ?>" method="post">*/?>
     <form id="dateSelect" action="/analytics/id/<?php echo $videoID; ?>" method="post" class="form-horizontal" >
        <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Week</label>
    <div class="col-sm-10">
         <input id="datepicker" class="week-picker" onchange='this.form.submit()' placeholder="<?php if(isset($startDate) && isset($endDate)) { echo $startDate.' - '.$endDate; } else { ?>Select a week<?php } ?>"></input> <!--<label>Week :</label> <span id="startDate"></span> - <span id="endDate"></span>-->
        <input type="hidden" id="startDate" name="startDate" value=""/>
        <input type="hidden" id="endDate"  name="endDate" value=""/>
      </div>
   </div>
    </form> 
     
   </div>
   <div class="col-md-3 col-xs-3">
     <?php
	    $get = $this->uri->ruri_to_assoc();
		$videoID = $get['id'];
		 
		?>
    <?php /* <form id="dateSelect" action="/analytics/id/<?php echo $videos[0]->videoid; ?>" method="post">*/?>
     <form id="dateSelect" action="/analytics/id/<?php echo $videoID; ?>" method="post" class="form-horizontal" >
        <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Graph</label>
    <div class="col-sm-10">
            <select name="checkwebID" id="checkwebID" onchange="window.open(this.options[this.selectedIndex].value,'_top')">
          <option value="">Impressions</option> 
             
             <option value="">Unique Visitors</option> 
             <option value="">Video Views</option>
             <option value="">CTA Clicks</option> 
             <option value="">Optin Submitted</option> 
             <option value="">Optin Conversion Rate</option>
             <option value="">CTA Conversion Rate</option>
            </select>
      </div>
   </div>
    </form> 
     
   </div>
    </div>
      <div class="col-md-2"></div>
    </div>
    
   </div>
 <div id="chart" style="width:100%; height:auto;padding-top:1em;"></div> 

 <table class="table" id="datatable" style="display:none;">
	<thead>
		<tr>
			<th>Day</th>
			<th>Unique Visitors</th> 
            <!--<th>Views</th>
            <th>CTA Clicks</th>
            <th>Opt In</th>
            <th>Opt In - Conversion Rate</th>
            <th>CTA - Conversion Rate</th>--> 
           
		</tr>
	</thead>
	<tbody>
    <?php foreach($analytics as $data) { ?>
       <?php $x = $data->views; $y = $data->optin; $v = $data->cta_click;
	   
	    if($data->optin > '0') {
	         $optRate = ($y/$x * 100);
		} else {
			$optRate = '0';
		}
		 if($data->cta_click > '0') {
	         $ctaRate = ($v/$x * 100);
		} else {
			$ctaRate = '0';
		}
			
	     ?>
         <tr>
			<th><?php echo  $data->day;?></th>
			<td><?php echo  $data->unique_views;?></td> 
           <?php /* <td> <?php echo  $data->views;?></td>
            <td> <?php echo  $data->cta_click;?></td>
            <td> <?php echo  $data->optin;?></td>
            <td><?php echo round($optRate);?></td>
            <td><?php echo round($ctaRate);?></td> */?>
         </tr>
         
	<?php } ?> 
 <?php $totalUnique = 0;  ?>
 <?php foreach($analytics as $data) { ?>
 <?php  $totalUnique += $data->unique_views;?>
 <?php } ?> 
 <?php $totalViews = 0;  ?>
 <?php foreach($analytics as $data) { ?>
 <?php  $totalViews += $data->views;?>
 <?php } ?> 
  <?php $totalCta = 0;  ?>
 <?php foreach($analytics as $data) { ?>
 <?php  $totalCta += $data->cta_click;?>
 <?php } ?> 
 <?php $totalOpt = 0;  ?>
 <?php foreach($analytics as $data) { ?>
 <?php  $totalOpt += $data->optin;?>
 <?php } ?> 
 
<?php $x = $totalViews; $y = $totalOpt; $v = $totalCta;
	         $totalOptRate = ($y/$x * 100);
			 $totalCtaRate = ($v/$x * 100);
	     ?>
     <?php /*       <tr>
          <th>Total</th>
			<td><?php echo $totalUnique;?></td>
			<td><?php echo $totalViews;?></td> 
            <td><?php echo $totalCta;?></td>
            <td><?php echo $totalOpt;?></td>
             <td><?php echo round($totalOptRate);?></td>
            <td><?php echo round($totalCtaRate);?></td>
       </tr>  */ ?>
	</tbody>
</table> 
 

 <div class="row analtyic_totals"> 
  <div class="col-md-2 text-center">
  <h5 class="analtyic_totals_title">Unique Visitors</h5>
  <h1 class="analtyic_total_num"><?php echo $totalUnique;?></h1>
  </div><!--/col-->
  <div class="col-md-2 text-center">
    <h5 class="analtyic_totals_title">Video Views</h5>
  <h1><?php echo $totalViews;?></h1>
  </div><!--/col-->
  <div class="col-md-2 text-center">
   <h5 class="analtyic_totals_title">CTA Clicks</h5>
  <h1><?php echo $totalCta;?></h1>
  </div><!--/col-->
  <div class="col-md-2 text-center">
   <h5 class="analtyic_totals_title">Optin Submitted</h5>
  <h1><?php echo $totalOpt;?></h1>
  </div><!--/col-->
  <div class="col-md-2 text-center">
   <h5 class="analtyic_totals_title">Optin Conversion Rate</h5>
  <h1><?php echo round($totalOptRate);?>%</h1>
  </div><!--/col-->
  <div class="col-md-2 text-center border-none">
   <h5 class="analtyic_totals_title">CTA Conversion Rate</h5>
  <h1><?php echo round($totalCtaRate);?>%</h1>
  </div><!--/col-->
 </div>
 
      </div><!--/col-md-6 col-xs-6-->
      
        </div>
 </div>    
 
 </div>
  </div>
         
 </div>
  
</div>
</div>
</section>


<div class="push"></div>
</div>
<footer style="background:#f9fcfd;border-top:1px solid #dddfdf;">
<div class="container-fluid">  
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div>
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<a href="http://www.yazamo.com/" target="_blank"><img src="<?php echo $base;?>images/copyright.jpg"/></a>
</div>
</div>
<div class="col-md-4 col-xs-4">
</div>
</div>
 </footer> 
 
 
</div>
<div class="helpBtn">
<a href="#"></a>
</div>
<!-- add category -->
 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
  <script src="<?php echo $base;?>js/highcharts.js"></script>
  <script src="<?php echo $base;?>js/js/modules/data.js"></script>
<script src="<?php echo $base;?>js/js/modules/exporting.js"></script>
  <script>
 $(function () {
    $('#chart').highcharts({
		
        data: {
            table: document.getElementById('datatable')
        },
        chart: {
			backgroundColor: '#f9fcfd',
            type: 'line'
        },
        title: {
            text: '<?php //echo $video[0]->video_title;?> '
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: ''
            }
        },
	   series: [{
		  
		 showInLegend: false,               
			
	}],

		tooltip: {
                formatter: function() {
                    return '<b>'+ this.series.name + '</b> - '+
                        this.point.y +
                        (this.series.name == 'Opt In - Conversion Rate'||this.series.name =='CTA - Conversion Rate' ? '%' : '');
                }
				
			
           /* formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+
                    this.point.y +'%';
            }
       /* tooltip: {
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+
                    this.point.y +' '+ this.point.name.toLowerCase();
            }*/
        } 
		
		 
    });
	

 
          
});
  </script>

 
  <script type="text/javascript">
            $(function() {
			 
				var startDate;
                var endDate;

                var selectCurrentWeek = function() {
                    window.setTimeout(function() {
                        $('.ui-weekpicker').find('.ui-datepicker-current-day a').addClass('ui-state-active').removeClass('ui-state-default');
                    }, 1);
                }

                var setDates = function(input) {
                    var $input = $(input);
                    var date = $input.datepicker('getDate');
                    if (date !== null) {
                        var firstDay = $input.datepicker("option", "firstDay");
                        var dayAdjustment = date.getDay() - firstDay;
                        if (dayAdjustment < 0) {
                            dayAdjustment += 7;
                        }
                        startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - dayAdjustment);
                        endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - dayAdjustment + 6);

                        var inst = $input.data('datepicker');
                        var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
                        $('#startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
                        $('#endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
						$('input#startDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
                        $('input#endDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
                    }
                }

                var week_selector = function() {
                    var $calendarTR = $('.ui-weekPicker .ui-datepicker-calendar tr');
                    $calendarTR.on('mousemove', function() {
                        $(this).find('td a').addClass('ui-state-hover');
                    });
                    $calendarTR.on('mouseleave', function() {
                        $(this).find('td a').removeClass('ui-state-hover');
                    });
                }

                $('.week-picker').datepicker({
				   
				   dateFormat: 'mm/dd/yy',
				   minDate: new Date('05/01/2014'),
				   maxDate: new Date(),
                   
				   beforeShow: function() {
                        $('#ui-datepicker-div').addClass('ui-weekpicker');
                        selectCurrentWeek();
                        window.setTimeout(function() {
                            week_selector();
                        }, 10);
                    },
                    onClose: function() {
                        $('#ui-datepicker-div').removeClass('ui-weekpicker');
                    },
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    onSelect: function(dateText, inst) {
                        setDates(this);
                        selectCurrentWeek();
                        $(this).change();
                    },
                    beforeShowDay: function(date) {
                        var cssClass = '';
                        if (date >= startDate && date <= endDate) cssClass = 'ui-datepicker-current-day';
                        week_selector();
                        return [true, cssClass];
                    },
                    onChangeMonthYear: function(year, month, inst) {
                        selectCurrentWeek();
                        window.setTimeout(function() {
                            week_selector();
                        }, 10);
                    }
					
				
                });

                setDates('.week-picker');

            });
        </script>
        <script>
 
<?php /* 
 $("#dateSelect").change(function(e){
  e.preventDefault();
  var data = $('#dateSelect').serialize();
  $.ajax({
    type: "POST",
    url: "/analytics/id/<?php echo $videos[0]->videoid; ?>",
    data: data,
    success: function(){
	   
       $('.table') .load('/analytics/id/<?php echo $videos[0]->videoid; ?> .table', function() {
			  $('#chart') .load('/analytics/id/<?php echo $videos[0]->videoid; ?> #chart', function() {
			});
	    });
		
    }
    });
}); */?> 
</script>
</body>
</html>