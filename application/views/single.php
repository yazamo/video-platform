<?php $user = $this->ion_auth->user()->row(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BetterCapture - Video Library</title>
<?php $base = '/assets/';?>
<link rel="stylesheet" href="<?php echo $base;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/custom.css"/>
<link rel="stylesheet" href="<?php echo $base;?>css/fonts.css"/>
</head>
<body class="betterCapture">
<?php
 /* Get Video Time */
$vidID = $result[0]->url;
$url = "http://gdata.youtube.com/feeds/api/videos/". $vidID;
$doc = new DOMDocument;
$doc->load($url);
$vtitle = $doc->getElementsByTagName("title")->item(0)->nodeValue;
$duration = $doc->getElementsByTagName('duration')->item(0)->getAttribute('seconds');
$time = gmdate("H:i:s", $duration );
?>
<div class="wrapper">
<header>
 <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid" style="padding-right:0;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img class="logo" src="<?php echo $base;?>images/logo.png" alt="BetterCapture" title="BetterCapture"/></a>
    </div>
     <div class="collapse navbar-collapse">
      <div class="pull-right">
      <ul class="nav navbar-nav navbar-right">
       <li class="mainLink"><a href="/create_video">Add Video</a></li>
       <li class="mainLink"><a href="/">Video Library</a></li>
       <li class="mainLink"><a href="/analytic_dashboard">Analytics</a></li>
       <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
        <li class="dropdown register-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <!--<img class="userIcon" src="<?php //echo $base;?>images/userIcon.png"/>&nbsp;&nbsp;--> <?php echo $user->first_name;?> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="/settings"><img src="<?php echo $base;?>images/settingsIcon.jpg"/>&nbsp;&nbsp;Settings</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="#"><img src="<?php echo $base;?>images/helpIcon.jpg"/>&nbsp;&nbsp;Help</a></li>
             <li class="divider lightDiv"></li>
             <li class="divider darkDiv"></li>
            <li><a href="/logout"><img src="<?php echo $base;?>images/logoutIcon.jpg"/>&nbsp;&nbsp;Logout</a></li>
          </ul>
        </li>
       
      </ul>
      </div><!-- /.register -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>
<section class="filterBar" style="height:60px;">
<div class="container-fluid">
<div class="col-md-8 col-xs-8">
<div class="text-left">
<h1 class="pageTitle"><?php echo $title;?></h1>
</div><!--/text-left -->
</div><!--/col-md-8 col-xs-8 -->
<div class="col-md-4 col-xs-4">
</div><!--/col-md-4 col-xs-4 -->
</div><!--/container-fluid -->
</section>
<section class="mainSection">
<div class="container-fluid">
<div class="col-md-6 col-xs-6">
<!-- Nav tabs -->
<ul class="nav nav-tabs editTabs">
  <li class="active text-center"><a class="borderLeft" href="#options" data-toggle="tab">Options</a></li>
  <li class="text-center"><a href="#callToAction" data-toggle="tab">CTA</a></li>
  <li class="text-center"><a href="#optIn" data-toggle="tab">Opt-In</a></li>
  <li class="text-center"><a class="borderRight" href="#embed" data-toggle="tab">Embed</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content editContent">
  <div class="tab-pane active" id="options">
 <form action="" method="post" id="edit_video_form">
 <input type="hidden" value="<?php echo $result[0]->videoid; ?>" name="videoID"/>
 <input type="hidden" value="<?php echo $user->id;?>" name="userID"/>
   <div class="form-group">
    <label for="exampleInputEmail1">Video Title</label>
     <input type="text" name="video_title" id="video_title_input" value="<?php echo stripslashes($result[0]->video_title); ?>" class="form-control"/>
  </div><!--/form-group -->
  <div class="row">
 <div class="col-md-9">
  <div class="form-group">
    <label for="url">YouTube URL</label>
 	<input type="text" name="url" id="url_input" value="http://www.youtube.com/watch?v=<?php echo stripslashes($result[0]->url); ?>"class="form-control"/>
  </div><!--/form-group-->
  </div><!--/col-md-9 -->
  <div class="col-md-3">
  <div class="form-group">
 <label>Controls</label>
   <br>
  <?php if($result[0]->timeline == '0') { ?>
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-success">
      <input type="radio" name="timeline" value="1" id="timelineOn"> On
      </label>
      <label class="btn btn-danger active">
    <input type="radio" name="timeline" value="0" id="timelineOff" checked="checked"> Off
    </label>
     </div><!--/btn-group-->
<?php	 } else { ?>
<div class="btn-group" data-toggle="buttons">
   <label class="btn btn-success active">
   <input type="radio" name="timeline" value="1" id="timelineOn" checked="checked"> On
   </label>
   <label class="btn btn-danger">
    <input type="radio" name="timeline" value="0" id="timelineOff"> Off
    </label>
     </div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->
</div><!--/col-md-3 -->
</div><!--/row -->
 <div class="row">
 <div class="col-md-6">
    <div class="row">
  <div class="col-md-6">
  <div class="form-group">
    <label for="width">Width</label>
      	   <div class="input-group">
          <input type="text" name="width" id="width_input" value="<?php echo stripslashes($result[0]->width); ?>" class="form-control"/>
            <span class="input-group-addon">px</span>
            </div><!--/input-group-->
    </div><!--/form-group-->
    </div><!--/col-md-6-->
    <div class="col-md-6">
  <div class="form-group">
    <label for="height">Height</label>  
         <div class="input-group">
        <input type="text" name="height" id="height_input" value="<?php echo stripslashes($result[0]->height); ?>" class="form-control"/>
         <span class="input-group-addon">px</span>
         </div><!--/input-group-->
 </div><!--/form-group-->
 </div><!--/col-md-6-->
</div><!--/row-->
 </div><!--/col-md-4-->
 <div class="col-md-3">
   <div class="form-group">
      <label for="height">Autoplay</label>
      <br>
       <?php if($result[0]->autoplay == '0') { ?>
       <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-success">
        <input type="radio" name="autoplay" value="1" id="autoplayOn"> On
      </label>
      <label class="btn btn-danger active">
        <input type="radio" name="autoplay" value="0" id="autoplayOff" checked="checked"> Off
      </label> 
     </div><!--/btn-group-->
     <?php	 } else { ?>  
       <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-success active">
        <input type="radio" name="autoplay" value="1" id="autoplayOn" checked="checked"> On
        </label>
        <label class="btn btn-danger">
        <input type="radio" name="autoplay" value="0" id="autoplayOff"> Off
        </div><!--/btn-group-->
     <?php } ?>
     </div><!--/form-group-->
  </div><!--/col-md-3-->
 <div class="col-md-3">
  <div class="form-group">  
   <label>Enable HD</label>
   <br>
   <?php if($result[0]->hd == '0') { ?>
  <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-success">
    <input type="radio" name="hd" value="1" id="hdOn"> On
    </label>
     <label class="btn btn-danger active">
    <input type="radio" name="hd" value="0" id="hdOff" checked="checked"> Off
    </label>
    </div><!--/btn-group-->
<?php	 } else { ?>
  <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-success active">
    <input type="radio" name="hd" value="1" id="hdOn" checked="checked"> On
    </label>
     <label class="btn btn-danger">
    <input type="radio" name="hd" value="0" id="hdOff"> Off
    </label>
  </div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->
</div><!--/col-md-3-->
</div><!--/ -->
<div class="row">
<div class="col-md-12">
 <div class="form-group">  
 <label>Description</label>
<textarea class="form-control" rows="3" name="description" placeholder="Add a description for this video"><?php if ($result[0]->description != '0') { echo stripslashes($result[0]->description); } ?></textarea>
</div>
</div>
</div><!--/form-group-->

</div><!--/tab-->
<div class="tab-pane" id="callToAction">  
<div class="form-group">    
   <label>Call to Action</label>
   <br/>
 <?php if($result[0]->cta === '0') { ?>
    <div class="btn-group" data-toggle="buttons">
     <label class="btn btn-success" id="ctaSwitchOn">
     <input type="radio" name="cta" value="1" id="ctaOn"> On
     </label>
     <label class="btn btn-danger active" id="ctaSwitchOff">
     <input type="radio" name="cta" value="0" id="ctaOff" checked="checked"> Off
     </label>
    </div><!--/btn-group-->
<?php	 } else { ?>
 <div class="btn-group" data-toggle="buttons">
     <label class="btn btn-success active"  id="ctaSwitchOn">
     <input type="radio" name="cta" value="1" id="ctaOn" checked="checked"> On
     </label>
     <label class="btn btn-danger" id="ctaSwitchOff">
     <input type="radio" name="cta" value="0" id="ctaOff"> Off
     </label>
   </div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->
<div class="form-group" id="ctaText">
  <hr/>
   <div class="row">
     <div class="col-md-6">
     <label for="ctaText">CTA Text</label>
     </div>
     <div class="col-md-6">
     <label for="ctaText">CTA Link</label>
      </div>
     </div>
     <div class="row">
     <div class="col-md-6">
 	<input type="text" name="ctaText" id="cta_text_input" value="<?php if ($result[0]->ctaText != '0') { ?><?php echo stripslashes($result[0]->ctaText); ?><?php }else{echo '0';} ?>" class="form-control" placeholder="Enter CTA text" maxlength="46"/>
     </div>
     <div class="col-md-6">
      <div class="input-group">
      <!--<span class="input-group-addon">http://</span>-->
      <?php if($result[0]->ctaLink != '0') { ?>
     <input type="text" name="ctaLink" id="cta_link_input" value="<?php echo stripslashes($result[0]->ctaLink); ?>" class="form-control"/>
      <?php } else { ?>
      <input type="text" name="ctaLink" id="cta_link_input" value="0" class="form-control" placeholder="example.com"/>
      <?php } ?>
      </div>
    </div>
     </div>
 </div><!--/form-group--> 
 
   
<div class="form-group"  id="ctatime"> 
<hr/>
  <label>Call to action appears at</label>
  <br/>
<?php if($result[0]->ctatime === '0') { ?>
   <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-default" id="ctatimeOn">
    <input type="radio" name="ctatimeSwitch" value="1" id="ctatimeOn"> Start
    </label>
    <label class="btn btn-default active middleBtn"  id="ctatimeOff">
    <input type="radio" name="ctatimeSwitch" value="0" id="ctatimeOff" checked="checked"> End
    </label>
     <label class="btn btn-default" id="ctatimeCustom">
    <input type="radio" name="ctatimeSwitch" value="2" id="ctatimeCustom"> Custom
    </label>
    </div><!--/btn-group-->
<?php } elseif($result[0]->ctatime === '1') { ?>
   <div class="btn-group" data-toggle="buttons" id="ctatime">
   <label class="btn btn-default active"  id="ctatimeOn">
    <input type="radio" name="ctatimeSwitch" value="1" id="ctatimeOn" checked="checked"> Start
    </label>
    <label class="btn btn-default middleBtn"  id="ctatimeOff">
    <input type="radio" name="ctatimeSwitch" value="0" id="ctatimeOff"> End
    </label>
    <label class="btn btn-default" id="ctatimeCustom">
    <input type="radio" name="ctatimeSwitch" value="2" id="ctatimeCustom"> Custom
    </label>
   </div><!--/btn-group-->
<?php } else { ?>
<div class="btn-group" data-toggle="buttons" id="ctatime">
   <label class="btn btn-default" id="ctatimeOn">
    <input type="radio" name="ctatimeSwitch" value="1" id="ctatimeOn"> Start
    </label>
    <label class="btn btn-default middleBtn"  id="ctatimeOff">
    <input type="radio" name="ctatimeSwitch" value="0" id="ctatimeOff"> End
    </label>
    <label class="btn btn-default active" id="ctatimeCustom">
    <input type="radio" name="ctatimeSwitch" value="2" id="ctatimeCustom" checked="checked"> Custom
    </label>
   </div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->
 
<?php

  if($result[0]->ctatime === '0' || $result[0]->ctatime === '1')  { 
  
    list($hours,$mins,$secs) = explode(':',$time);

  } else {
	
	  //$t = round($result[0]->ctatime);
     //$ntime = sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
	 $ntime = $result[0]->ctatime;
	 list($hours,$mins,$secs) = explode(':',$ntime);
	
}
?>

 <div class="form-group" id="ctacustomtime">
  <hr/>
    <label for="url">Custom Time</label>
     <div class="row">
     <div class="col-md-4"><div class="input-group"><input id="hours" type="number" name="ctatime[]" value="<?php echo $hours;?>" class="form-control"/><span class="input-group-addon">Hours</span></div></div>
     <div class="col-md-4"><div class="input-group"><input id="mins" type="number" name="ctatime[]" value="<?php echo $mins;?>" class="form-control"/><span class="input-group-addon">Minutes</span></div></div>
     <div class="col-md-4"><div class="input-group"><input id="secs" type="number" name="ctatime[]" value="<?php echo $secs;?>" class="form-control"/><span class="input-group-addon">Seconds</span></div></div>
     </div>
   </div><!--/form-group-->
    
</div><!--/tab-->
<div class="tab-pane" id="optIn"> 
 <?php if($api_type[0]->api_type != '0') { ?>
 <div class="form-group" id="optData"> 
 <div class="row">
 <div class="col-md-3">  
 
<label>Opt-In</label>
<br/>
 <?php if($result[0]->opt === '0') { ?>
  <div class="btn-group" data-toggle="buttons">
   <label class="btn btn-success" id="optinSwitchOn">
  <input type="radio" name="opt" value="1" id="optOn"> On
  </label>
  <label class="btn btn-danger active" id="optinSwitchOff">
  <input type="radio" name="opt" value="0" id="optOff" checked="checked"> Off
  </label>
  </div><!--/btn-group-->
<?php	 } else { ?>
<div class="btn-group" data-toggle="buttons">
   <label class="btn btn-success active" id="optinSwitchOn">
   <input type="radio" name="opt" value="1" id="optOn" checked="checked"> On
   </label>
    <label class="btn btn-danger" id="optinSwitchOff">
    <input type="radio" name="opt" value="0" id="optOff"> Off
   </label>
</div><!--/btn-group-->
<?php } ?>
 </div>
<div class="col-md-9" id="integration">   

 <?php
 /* Check if IS List is set then display if true */
 if(isset($tags)) { ?>
   <label>CRM (Infusionsoft)</label>
   <br/>
  <select id="emailSelect" name="input_field[]" class="form-control">
  <?php if($current_list[0]->list_name != '0')  { ;?><option selected><?php echo $current_list[0]->list_name;?></option><?php  }?>
 
 <?php  foreach($tags as $data) : ?>
	  <?php if($current_list[0]->list_name === $data['GroupName'])  { ;?>
      <?php  } else { ?>
     <option  value="<?php echo $data['Id']; ?>"><?php echo $data['GroupName']; ?></option>
     <?php } ?>
   <?php endforeach; ?>	
   <input id="listName" name="input_field[]" type="hidden" value="<?php echo $tags[0]['GroupName'];?>" />
 </select>
 
<?php  } ?>

<?php
 /* Check if MC List is set then display if true */

 if(isset($getUserList)) { ?>
 <label>CRM (MailChimp)</label>
  <br/>
 <select id="emailSelect" name="input_field[]" class="form-control">
 <?php if($current_list[0]->list_name != '0')  { ;?><option selected><?php echo $current_list[0]->list_name;?></option><?php  }?>
 <?php  foreach ($getUserList['data'] as $listName => $data) : ?>
	 <?php if($current_list[0]->list_name === $data['name'])  { ;?>
      <?php  } else { ?>
     <option value="<?php echo $data['id'];?>"><?php echo $data['name'];?></option>
     <?php } ?> 
  <?php endforeach; ?>	
<input id="listName" name="input_field[]" type="hidden" value="<?php echo $getUserList['data'][0]['name'];?>" />
</select>
 <?php  } ?>
 <?php
 /* Check if CC List is set then display if true */
 if($api_type[0]->api_type === '3') { ?>

 <label>CRM (ConstantContact)</label>
  <br/>
 <select id="emailSelect" name="input_field[]" class="form-control">
 <?php if($current_list[0]->list_name != '0')  { ;?><option selected><?php echo $current_list[0]->list_name;?></option><?php  }?>
  <?php foreach($ccLists['lists'] as $list) : ?>
   <?php if($current_list[0]->list_name === $list->name)  { ;?>
      <?php  } else { ?>
     <option value="<?php echo $list->id;?>"><?php echo $list->name;?></option>
     <?php } ?> 
  <?php endforeach; ?>
<input id="listName" name="input_field[]" type="hidden" value="<?php echo $ccLists['lists'][0]->name;?>" />
</select>
 <?php  } ?>
 <?php
 /* Check if CM List is set then display if true */
 if($api_type[0]->api_type === '4') { ?>

<label>CRM (CampaignMonitor)  <?php if (count($cmClients) > 1) { ?><a href="javascript(void);" data-toggle="modal" data-target="#cmClientList" class="text-right" id="cmClientLink">Change Client</a> <?php } ?></label>
  <br/>
<?php if(!empty($getCMLists)) { ?> 
<select id="emailSelect" name="input_field[]" class="form-control">
<?php if($current_list[0]->list_name != '0')  { ;?><option value="<?php echo $current_list[0]->value;?>" selected><?php echo $current_list[0]->list_name;?></option><?php  }?>
<?php foreach($getCMLists as $CMLists) { ?>
   <?php if($current_list[0]->list_name === $CMLists->Name)  { ;?>
      <?php  } else { ?>
     <option class="selectOption" value="<?php echo $CMLists->ListID;?>"><?php echo $CMLists->Name;?></option>
     <?php } ?> 
  <?php } ?>
 <input id="listName" name="input_field[]" type="hidden" value="<?php echo $getCMLists[0]->Name;?>" /> 
 </select>
  <?php } else { ?>
   <p>It appears that you have either deleted your client list in CampaignMonitor or have not yet added a subscriber list. Please <a href="javascript(void);" data-toggle="modal" data-target="#cmClientList" class="text-right">select</a> a new one.</p>
  <?php } ?>
<?php } ?>
 </div>
 </div>
 </div>

<div class="form-group" id="optinTitle">
  <hr/>
  <div class="row">
     <div class="col-md-8">
     <label for="optinTitle">Opt-In Title</label>
     </div>
     <div class="col-md-4">
     <label for="optinTitle">Can they skip?</label>
     </div>
   </div>
   <div class="row">
     <div class="col-md-8">
 	<input type="text" name="optinTitle" id="optinTitleInput" value="<?php if($result[0]->optinTitle != '0') { ?><?php echo stripslashes($result[0]->optinTitle); ?><?php }else{echo '0';} ?>" class="form-control" placeholder="Enter Opt In Text" maxlength="38"/>
    </div>
     <div class="col-md-4">
       <?php if($result[0]->allowSkip === '1') { ?>
          <div class="btn-group" data-toggle="buttons">
           <label class="btn btn-success active" id="allowSkip">
          <input type="radio" name="allowSkip" value="1" id="skipFormYes"  checked="checked"> Yes
          </label>
          <label class="btn btn-danger" id="optinSwitchOff">
          <input type="radio" name="allowSkip" value="0" id="skipFormNo"> No
          </label>
          </div><!--/btn-group-->
        <?php	 } else { ?>
        <div class="btn-group" data-toggle="buttons">
           <label class="btn btn-success" id="allowSkip">
           <input type="radio" name="allowSkip" value="1" id="skipFormYes"> Yes
           </label>
            <label class="btn btn-danger  active" id="optinSwitchOff">
            <input type="radio" name="allowSkip" value="0" id="skipFormNo" checked="checked"> No
           <label>
        </div><!--/btn-group-->
        <?php } ?>
       </div>
   </div>
    </div><!--/form-group--> 

<div class="form-group"  id="optintime"> 
<hr/>
  <label>Opt-In appears at</label>
  <br/>
<?php if($result[0]->optintime === '0') { ?>
   <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-default" id="optintimeOn">
    <input type="radio" name="optintimeSwitch" value="1" id="optintimeOn"> Start
    </label>
    <label class="btn btn-default active middleBtn"  id="optintimeOff">
    <input type="radio" name="optintimeSwitch" value="0" id="optintimeOff" checked="checked"> End
    </label>
     <label class="btn btn-default" id="optintimeCustom">
    <input type="radio" name="optintimeSwitch" value="2" id="optintimeCustom"> Custom
    </label>
    </div><!--/btn-group-->
<?php } elseif($result[0]->optintime === '1') { ?>
   <div class="btn-group" data-toggle="buttons" id="optintime">
   <label class="btn btn-default active"  id="ctatimeOn">
    <input type="radio" name="optintimeSwitch" value="1" id="optintimeOn" checked="checked"> Start
    </label>
    <label class="btn btn-default middleBtn"  id="optintimeOff">
    <input type="radio" name="optintimeSwitch" value="0" id="optintimeOff"> End
    </label>
    <label class="btn btn-default" id="optintimeCustom">
    <input type="radio" name="optintimeSwitch" value="2" id="optintimeCustom"> Custom
    </label>
   </div><!--/btn-group-->
<?php } else { ?>
<div class="btn-group" data-toggle="buttons" id="optintime">
   <label class="btn btn-default" id="optintimeOn">
    <input type="radio" name="optintimeSwitch" value="1" id="optintimeOn"> Start
    </label>
    <label class="btn btn-default middleBtn"  id="optintimeOff">
    <input type="radio" name="optintimeSwitch" value="0" id="optintimeOff"> End
    </label>
    <label class="btn btn-default active" id="optintimeCustom">
    <input type="radio" name="optintimeSwitch" value="2" id="optintimeCustom" checked="checked"> Custom
    </label>
   </div><!--/btn-group-->
<?php } ?>
</div><!--/form-group-->

<?php

  if($result[0]->optintime === '0' || $result[0]->optintime === '1')  { 
  
    list($ohours,$omins,$osecs) = explode(':',$time);

  } else {
	
	  //$t = round($result[0]->ctatime);
     //$ntime = sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
	 $otime = $result[0]->optintime;
	 list($ohours,$omins,$osecs) = explode(':',$otime);
	
}
?>

 <div class="form-group" id="optincustomtime">
  <hr/>
    <label for="url">Custom Time</label>
     <div class="row">
     <div class="col-md-4"><div class="input-group"><input id="ohours" type="number" name="optintime[]" value="<?php echo $ohours;?>" class="form-control"/><span class="input-group-addon">Hours</span></div></div>
     <div class="col-md-4"><div class="input-group"><input id="omins" type="number" name="optintime[]" value="<?php echo $omins;?>" class="form-control"/><span class="input-group-addon">Minutes</span></div></div>
     <div class="col-md-4"><div class="input-group"><input id="osecs" type="number" name="optintime[]" value="<?php echo $osecs;?>" class="form-control"/><span class="input-group-addon">Seconds</span></div></div>
     </div>
   </div><!--/form-group-->
   <?php  } else { ?>
  <div class="form-group"> 
 <label>Please set up your integration settings <a href="/settings">here</a> to enable opt in.</label>
   </div>
   <?php  } ?>
 </div><!--/tab-->   
 
  <div class="tab-pane" id="embed">
<code class="html"> <span class="nt">&ltdiv id="wd_id">&lt/div> </span>
<span class="s">&ltscript type="text/javascript" src="http://dev.bettercapture.com/assets/js/yzplayer.js">&lt/script> </span>
<span class="s">&ltscript type="text/javascript"> 
		init_widget(<?php echo $result[0]->userid; ?>,<?php echo $result[0]->videoid; ?>,<?php echo $api_type[0]->api_type; ?>,<?php echo $result[0]->width; ?>,<?php echo $result[0]->height; ?>)
	    &lt/script> </span></code>
      </div><!--/embed tab-->
   
 <div style="background:#FFF;" class="text-center submitPanel">
  <button type="submit" value="Save Changes" name="submit" class="btn btn-default">Save</button>
  <a href="/" class="btn btn-default">Back to Videos</a>
  <!--<button type="button" data-toggle="modal" data-target="#myModal-<?php echo $result[0]->videoid; ?>" id="modalLink-<?php echo $result[0]->videoid; ?>" class="btn btn-default">Preview</button>-->
</div><!--/btn-group btn-group-lg-->
 </div><!--/tab-content-->
 </form>
 <br/>
</div><!--/col-md-6-->
 <div class="col-md-6 col-xs-6">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo stripslashes($result[0]->video_title); ?> <a style="color: #3fb58e;" target="_blank" href="/full_preview/id/<?php echo $result[0]->videoid;?>" class="pull-right">Full Preview (Opens New Tab)</a></h3>
  </div>
  <div class="panel-body">
   <div id="wd_id"></div>
   <script type="text/javascript" src="https://dev.bettercapture.com/assets/js/bcpreview.js"></script><script type="text/javascript">init_widget(<?php echo $result[0]->userid; ?>,<?php echo $result[0]->videoid; ?>,<?php echo $api_type[0]->api_type; ?>,500,300);</script>
   </div><!--/ -->
</div><!--/ -->
</div><!--/-->
</div><!--/ -->
</section><!--/ -->

<div class="push"></div><!--/ -->
</div><!--/ -->
</div><!--/ -->
</section><!--/ -->

<footer style="background:#f9fcfd;border-top:1px solid #dddfdf;">
<div class="container-fluid">  
<div class="col-md-4 col-xs-4">
<ul class="footerLinks">
<li><a href="#"><img src="<?php echo $base;?>images/twitterIcon.jpg"/> <span>Twitter</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/facebookIcon.jpg"/> <span>Facebook</span></a></li>
<li><a href="#"><img src="<?php echo $base;?>images/blogIcon.jpg"/> <span>Blog</span></a></li>
</ul>
</div><!--/ -->
<div class="col-md-4 col-xs-4">
<div style="text-align:center;padding-top:.8em;">
<a href="http://www.yazamo.com/" target="_blank"><img src="<?php echo $base;?>images/copyright.jpg"/></a>
</div><!--/ -->
</div><!--/ -->
<div class="col-md-4 col-xs-4">
</div><!--/ -->
</div><!--/ -->
 </footer> 
 </div><!--/ -->
<div class="helpBtn">
<a href="#"></a>
</div><!--/ -->

<?php /* Check if campaignmonitor is on, if so check if user has more than 1 client list, if true the create modal to allow user to change client list*/?>
<?php if($api_type[0]->api_type === '4') { ?>
 <?php if(count($cmClients) > 1 || empty($getCMLists)) { ?>
<!-- CampaignMonitor Client List  -->
 <div id="cmClientList" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Client List</h4>
      </div>
      <div class="modal-body">
      <form id="setCmClient">
      <select class="form-control" name="clientID">
        <?php foreach($cmClients as $object) { ?>
         <option value="<?php echo $object->ClientID;?>"><?php  echo $object->Name;?></option>
       <?php   } ?> 
      </select>
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
       </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>  
  <?php } ?> 
<?php } ?> 
<?php /* End CampaignMonitor Client List Modal */?>
 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script src="<?php echo $base;?>js/bootstrap.min.js"></script>
 <script src="<?php echo $base;?>js/formValidation.js"></script>
 <script type='text/javascript'>


/* Grabs the list name and sets the value on hidden field for the php array */
$(function() {
    $('#emailSelect').change(function() {
      
        var x =  $('#emailSelect option:selected').text();
       
         $('#listName').val(x);
    });
});


/************/

/* If custom time is selected display input fields to insert custom time HH:MM:SS */

/* CTA */
<?php if($result[0]->ctatime == '0' || $result[0]->ctatime == '1')  { ?>
$( "#ctacustomtime" ).hide();
<?php } ?>
$( "#ctatimeCustom" ).click(function() {
   
   $( "#ctacustomtime" ).show();
});

$( "#ctatimeOn" ).click(function() {
    $( "#ctacustomtime" ).hide();
    
});
$( "#ctatimeOff" ).click(function() {
    $( "#ctacustomtime" ).hide();
    
});

$( "#ctatimeOff" ).click(function() {
    $( "#ctacustomtime" ).hide();
    
});


   
<?php if($result[0]->cta != '0') { ?>
	//$("#ctaLink").prop('required',true);
	//$("#ctaTextInput").prop('required',true);
<?php } ?>
<?php if($result[0]->cta === '0') { ?>
    $( "#ctaText" ).hide();
	$( "#ctatime" ).hide();
	$( "#ctacustomtime" ).hide();
	//$("#ctaLink").removeProp('required',true);
   // $("#ctaTextInput").removeProp('required',true);
<?php } ?>

$( "#ctaSwitchOff" ).click(function() {
	$('#ctaText :input').each(function(){
		$(this).val('0');
	});
    $( "#ctaText" ).hide();
	$( "#ctatime" ).hide();
	$( "#ctacustomtime" ).hide();
	//$("#ctaLink").removeProp('required',true);
    //$("#ctaTextInput").removeProp('required',true);
});
 

$( "#ctaSwitchOn" ).click(function() {
	$('#ctaText :input').each(function(){
		$(this).val('');
	})
	$( "#ctaText" ).show();
    $( "#ctatime" ).show();
	//$("#ctaLink").prop('required',true);
	//$("#ctaTextInput").prop('required',true);
    
});
 
 
/* OPTIN */

<?php if($result[0]->optintime == '0' || $result[0]->optintime == '1')  { ?>
$( "#optincustomtime" ).hide();
<?php } ?>

$( "#optintimeCustom" ).click(function() {
   
   $( "#optincustomtime" ).show();
});

$( "#optintimeOn" ).click(function() {
    $( "#optincustomtime" ).hide();
    
});
$( "#optintimeOff" ).click(function() {
    $( "#optincustomtime" ).hide();
    
});

 
<?php if($result[0]->opt === '0') { ?>
    $( "#optinTitle" ).hide();
	$( "#optintime" ).hide();
	$( "#optincustomtime" ).hide();
	 $( "#integration" ).hide();
	
<?php } ?>

   $( "#optinSwitchOff" ).click(function() {
   $('#optinTitleInput').each(function(){
		$(this).val('0');
	});
   $( "#optinTitle" ).hide();
   $( "#optintime" ).hide();
   // $("#optinTitleInput").removeProp('required',true);
   $( "#integration" ).hide();
   $( "#optincustomtime" ).hide();
    
});

  $( "#optinSwitchOn" ).click(function() {
  $('#optinTitleInput').each(function(){
		$(this).val('');
	});
  $( "#optinTitle" ).show();
  $( "#optintime" ).show();
  $( "#integration" ).show();
   //$("#optinTitleInput").prop('required',true);
 
});

<?php /* 
 <?php if($api_type[0]->api_type != '0') { ?>
  
 $("#emailSelect option[class='selectOption'][value='<?php echo $current_list[0]->value;?>']").hide();
  
<?php } ?>
*/ ?>

<?php if($api_type[0]->api_type === '4') { ?>
 <?php if(count($cmClients) > 1 || empty($getCMLists)) { ?>
$("#setCmClient").submit(function(e){
  e.preventDefault();
  var data = $('#setCmClient').serialize();
  $.ajax({
    type: "POST",
    url: "/cm_change_client",
    data: data,
    success: function(){
	  $('#cmClientList').modal('hide')
      $('#optData').load('/<?php echo uri_string();?> #optData', function() {
			/// can add another function here
	  });
    }
    });
});
<?php } ?>
<?php } ?>



/*
if($(".tab-content").find("div.tab-pane.active:has(required)").length == 0)
{
    $(".tab-content").find("div.tab-pane:hidden:has(div.has-error)").each(function(index, tab)
    {
        var id = $(tab).attr("id");

        $('a[href="#' + id + '"]').tab('show');
    });
}


/************ /



/* Make sure user does not input time on cta/optin higher than video */

<?php /* bugged need to fix and redo completely *******

$(function(){
    $("#hours").prop('min',0);
    $("#hours").prop('max',<?php echo $hours;?>);
	
	$("#mins").prop('min',0);
    $("#mins").prop('max',<?php echo $mins;?>);
	
	$("#secs").prop('min',0);
    $("#secs").prop('max',59);
});

/************/
?>
</script>
 <script type="text/javascript">
 (function() {
   var s = document.createElement("script");
   s.type = "text/javascript";
   s.async = true;
   s.src = '//api.usersnap.com/load/b757bce7-66b1-48d4-afe7-f6d9a6663289.js';
   var x = document.getElementsByTagName('head')[0];
   x.appendChild(s);
 })();
</script>
</body>
</html>
