$(function(){

	var inputs = {

			video_title_input: $('#video_title_input'),

			url_input: $('#url_input'),

			width_input: $('#width_input'),

			height_input: $('#height_input'),

			cta_text_input: $('#cta_text_input'),

			cta_link_input: $('#cta_link_input'),

			optinTitleInput: $('#optinTitleInput'),

		},

		edit_video_form = $('#edit_video_form');

		

	function errorSpots(inp, empty){

		var tab = [],

			tab_has_error = [];

		

		$.each(inp, function(i){

			if(empty == true)

				inputs[inp[i]].addClass('inputError');

			else

				inputs[inp[i]].removeClass('inputError');

			

			if(inputs[inp[i]].parent().parent().attr('id') == 'edit_video_form'

				|| inputs[inp[i]].parent().parent().parent().parent().attr('id') == 'edit_video_form'

				|| inputs[inp[i]].parent().parent().parent().parent().parent().parent().parent().attr('id') == 'edit_video_form'){

				tab.push('options');

			}else if(inputs[inp[i]].parent().parent().parent().parent().attr('id') == 'ctaText'){

				tab.push(inputs[inp[i]].parent().parent().parent().parent().parent().attr('id'));

			}else{

				tab.push(inputs[inp[i]].parent().parent().parent().parent().attr('id'));

			}

		});

		

		$.each(tab, function(i, v){

			tab_has_error.push(v);

			if($.inArray(v, tab_has_error) >= 0 && empty == true){

				$('.nav-tabs li a[href=#'+v+']').addClass('tabError');

			}else{

				$('.nav-tabs li a[href=#'+v+']').removeClass('tabError');

			}

		});

		

		if(empty == true)

			$('.nav-tabs li a[href=#'+tab[0]+']').trigger('click');

	}

		

	edit_video_form.submit(function(e){

		var empty_val = [],

			empty_val_inputs = [],

			filled_inputs = [];

		

		$.each(inputs, function(i, v){

			if(v.val() == ''){

				empty_val.push(true);

				empty_val_inputs.push(i);

			}else{

				empty_val.push(false);

				filled_inputs.push(i)

			}

		});

		

		if(inputs['url_input'].val().substr(0,4) != 'http'){

			empty_val.push(true);

			empty_val_inputs.push('url_input');

		}else if(inputs['cta_link_input'].val().indexOf('.') == '-1' && inputs['cta_link_input'].val() != '0'){

			empty_val.push(true);

			empty_val_inputs.push('cta_link_input');

		}

		

		if($.inArray(true, empty_val) != '-1'){

			errorSpots(filled_inputs, false);

			errorSpots(empty_val_inputs, true);

			return false;

		}

	})

})